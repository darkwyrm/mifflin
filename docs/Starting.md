# The Road Ahead

Wow. Based on the main design document, Mifflin is going to be a **lot** of work, but it doesn't have to be that way. Features can be built along the way. Once the basics are implemented, it should be a matter of incremental stuff from there.

## Development Stages

### Stage 1: The Bare Essentials

- primitive types: string, int, float, bool
- variables, including late binding (default `undefined` assignment) and constants
- functions
- main()
- comments
- operators: arithmetic, comparison, assignment
- basic flow control
	- if
	- while
	- break
	- continue
- stdlib:
	- print()/println()
	- console input: prompt()
	- exit()
	- panic()

### Stage 2: Classes, Containers, and Types, Oh My!

- Static typing
- Classes
- String becomes an Object type
- List
- Dictionary
- Ranges
- for()
- Command-line arguments
- Shell syntax?
- stdlib:
	- expand capabilities of existing built-ins
	- basic file I/O
	- basic networking
	- basic string manipulation
	- basic math

### Stage 3: Types

- Result
- when
- stdlib:
	- regular expressions

