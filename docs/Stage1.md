# Stage 1: The Bare Essentials

## Language Feature Notes

primitive types: string, int, float, bool

variables: `var myvar` or `var myvar = 10`

functions:
```
# a sample function with a comment
fn sample() -> string {

}
```

```
# Sample while loop
while true {
	# do something
}

# Sample of if/if-else
if x > 5 {
	print(x)
} else {
	print("Something else)
}
```
`break` and `continue` are options in the `while` loop

## Operators

- Arithmetic
	- addition: a + b
	- subtraction: a - b
	- multiplication: a * b
	- division: a / b
	- modulus: a % b
	- exponentiation: a ** b
- Comparison
	- structural (in)equality: ==, !=
	- referential (in)equality: ===, !==
	- greater than: >
	- less than: <
	- greater than or equal to: >=
	- less than or equal to: <=
- Assignment
	- assignment: =
	- assign with addition: a += b
	- assign with subtraction: a -= b
	- assign with multiplication: a *= b
	- assign with division: a /= b
	- assign with modulus: a %= b
- Logical
	- logical AND: and
	- logical OR: or
	- logical NOT: !

## Standard Library

- String concatenation
- Console input: `const foo = prompt(myprompt)`
- print()/println()
- type()
