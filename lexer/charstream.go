package lexer

// The CharStream type encapsulates the state needed to tokenize a string of source code.
type CharStream struct {
	Source       []rune
	startIndex   int
	currentIndex int
}

// Set sets the current source for the reader
func (cs *CharStream) Set(s string) {
	cs.Source = []rune(s)
	cs.startIndex = 0
	cs.currentIndex = 0
}

// StartNext sets the start of the next string to the current position
func (cs *CharStream) StartNext() {
	cs.startIndex = cs.currentIndex
}

// Substring returns the current string of characters
func (cs *CharStream) Substring() string {
	return string(cs.Source[cs.startIndex:cs.currentIndex])
}

// Advance consumes the current character and
func (cs *CharStream) Advance() rune {
	cs.currentIndex++
	return cs.Source[cs.currentIndex-1]
}

// AdvanceIf verifies that the current token matches the one expected and advances to the next
// character if it does.
func (cs *CharStream) AdvanceIf(expected rune) bool {
	if cs.IsAtEnd() || cs.Source[cs.currentIndex] != expected {
		return false
	}
	cs.currentIndex++
	return true
}

func (cs *CharStream) Peek() rune {
	if cs.IsAtEnd() {
		return '\000'
	} else {
		return cs.Source[cs.currentIndex]
	}
}

func (cs *CharStream) PeekNext() rune {
	if cs.currentIndex+1 >= len(cs.Source) {
		return '\000'
	} else {
		return cs.Source[cs.currentIndex+1]
	}
}

// IsAtEnd returns true if at the end of the input source
func (cs *CharStream) IsAtEnd() bool {
	return cs.currentIndex >= len(cs.Source)
}
