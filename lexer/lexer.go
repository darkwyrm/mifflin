package lexer

import (
	"strings"
	"unicode"

	"gitlab.com/darkwyrm/mifflin/tokens"
)

type Lexer struct {
	reader      CharStream
	currentLine int
}

func New(source string) *Lexer {
	lex := &Lexer{}
	lex.reader.Set(source)
	lex.currentLine = 0
	return lex
}

// Tokenize returns the entire set of tokens for input text. For on-demand tokenization,
// use NextToken().
func (lex *Lexer) Tokenize(source string) []tokens.Token {
	out := make([]tokens.Token, 0)

	lex.Set(source)
	for {
		token := lex.NextToken()
		out = append(out, token)
		if token.Type == tokens.EOF {
			break
		}
	}
	return out
}

// Set resets the lexer to a new set of source code.
func (lex *Lexer) Set(source string) {
	lex.reader.Set(source)
	lex.currentLine = 0
}

// NextToken returns the next token in the input source code. Requires Set() before use.
func (lex *Lexer) NextToken() tokens.Token {

	if lex.reader.IsAtEnd() {
		return tokens.Token{Type: tokens.EOF, Literal: "", Line: lex.currentLine}
	}

	lex.skipWhitespace()
	lex.reader.StartNext()

	for !lex.reader.IsAtEnd() {
		c := lex.reader.Advance()
		switch c {
		case '(':
			return tokens.Token{Type: tokens.LPAREN, Literal: "(", Line: lex.currentLine}
		case ')':
			return tokens.Token{Type: tokens.RPAREN, Literal: ")", Line: lex.currentLine}
		case '[':
			return tokens.Token{Type: tokens.LBRACKET, Literal: "[", Line: lex.currentLine}
		case ']':
			return tokens.Token{Type: tokens.RBRACKET, Literal: "]", Line: lex.currentLine}
		case '{':
			return tokens.Token{Type: tokens.LBRACE, Literal: "{", Line: lex.currentLine}
		case '}':
			return tokens.Token{Type: tokens.RBRACE, Literal: "}", Line: lex.currentLine}
		case ',':
			return tokens.Token{Type: tokens.COMMA, Literal: ",", Line: lex.currentLine}
		case '.':
			return tokens.Token{Type: tokens.DOT, Literal: ".", Line: lex.currentLine}
		case ':':
			return tokens.Token{Type: tokens.COLON, Literal: ":", Line: lex.currentLine}
		case '-':
			if lex.reader.AdvanceIf('=') {
				return tokens.Token{Type: tokens.MINUSEQ, Literal: "-=", Line: lex.currentLine}
			}
			return tokens.Token{Type: tokens.MINUS, Literal: "-", Line: lex.currentLine}
		case '+':
			if lex.reader.AdvanceIf('=') {
				return tokens.Token{Type: tokens.PLUSEQ, Literal: "+=", Line: lex.currentLine}
			}
			return tokens.Token{Type: tokens.PLUS, Literal: "+", Line: lex.currentLine}
		case '%':
			if lex.reader.AdvanceIf('=') {
				return tokens.Token{Type: tokens.PERCENTEQ, Literal: "%=", Line: lex.currentLine}
			}
			return tokens.Token{Type: tokens.PERCENT, Literal: "%", Line: lex.currentLine}
		case ';':
			return tokens.Token{Type: tokens.SEMICOLON, Literal: ";", Line: lex.currentLine}
		case '*':
			if lex.reader.AdvanceIf('*') {
				return tokens.Token{Type: tokens.DBLSTAR, Literal: "**", Line: lex.currentLine}
			}
			if lex.reader.AdvanceIf('=') {
				return tokens.Token{Type: tokens.STAREQ, Literal: "*=", Line: lex.currentLine}
			}
			return tokens.Token{Type: tokens.STAR, Literal: "*", Line: lex.currentLine}
		case '/':
			if lex.reader.AdvanceIf('=') {
				return tokens.Token{Type: tokens.SLASHEQ, Literal: "/=", Line: lex.currentLine}
			}
			return tokens.Token{Type: tokens.SLASH, Literal: "/", Line: lex.currentLine}

		case '!':
			if lex.reader.AdvanceIf('=') {
				return tokens.Token{Type: tokens.BANGEQ, Literal: "!=", Line: lex.currentLine}
			}
			return tokens.Token{Type: tokens.BANG, Literal: "!", Line: lex.currentLine}
		case '=':
			if lex.reader.AdvanceIf('=') {
				return tokens.Token{Type: tokens.EQEQ, Literal: "==", Line: lex.currentLine}
			}
			return tokens.Token{Type: tokens.EQ, Literal: "=", Line: lex.currentLine}
		case '<':
			if lex.reader.AdvanceIf('=') {
				return tokens.Token{Type: tokens.LTEQ, Literal: "<=", Line: lex.currentLine}
			}
			return tokens.Token{Type: tokens.LT, Literal: "<", Line: lex.currentLine}
		case '>':
			if lex.reader.AdvanceIf('=') {
				return tokens.Token{Type: tokens.GTEQ, Literal: ">=", Line: lex.currentLine}
			}
			return tokens.Token{Type: tokens.GT, Literal: ">", Line: lex.currentLine}

		case '"':
			return lex.readString()

		case '#':
			for lex.reader.Peek() != '\n' && !lex.reader.IsAtEnd() {
				lex.reader.Advance()
			}
			lex.skipWhitespace()
		default:
			switch {
			case isDigitChar(c):
				return lex.readNumber()
			case isIDChar(c):
				return lex.readIdentifier()
			default:
				return tokens.Token{Type: tokens.ILLEGAL, Literal: "", Line: lex.currentLine}
			}
		}
	}

	// We should only get here when we've reached the end of the source text
	return tokens.Token{Type: tokens.EOF, Literal: "", Line: lex.currentLine}
}

func (lex *Lexer) skipWhitespace() {
	for unicode.IsSpace(lex.reader.Peek()) {
		if lex.reader.Peek() == '\n' {
			lex.currentLine++
		}
		lex.reader.Advance()
		if lex.reader.IsAtEnd() {
			return
		}
	}
	lex.reader.StartNext()
}

// Returns a token representing a number
func (lex *Lexer) readNumber() tokens.Token {
	for isDigitChar(lex.reader.Peek()) {
		lex.reader.Advance()
	}

	if lex.reader.Peek() == 'u' || lex.reader.Peek() == 'U' {
		processed := strings.ReplaceAll(lex.reader.Substring(), "_", "")
		out := tokens.Token{Type: tokens.UINT, Literal: processed, Line: lex.currentLine}
		lex.NextToken()
		return out
	}

	if lex.reader.Peek() == '.' && isDigitChar(lex.reader.PeekNext()) {
		lex.reader.Advance()
		for isDigitChar(lex.reader.Peek()) {
			lex.reader.Advance()
		}
		processed := strings.ReplaceAll(lex.reader.Substring(), "_", "")
		return tokens.Token{Type: tokens.FLOAT, Literal: processed, Line: lex.currentLine}
	}

	processed := strings.ReplaceAll(lex.reader.Substring(), "_", "")
	return tokens.Token{Type: tokens.INT, Literal: processed, Line: lex.currentLine}
}

// Reads the current source and returns an identifier token
func (lex *Lexer) readIdentifier() tokens.Token {

	// Identifiers must be ASCII in order to prevent Trojan Source attacks
	for isIDChar(lex.reader.Peek()) {
		lex.reader.Advance()
	}
	return tokens.Token{Type: tokens.StringToTokenType(lex.reader.Substring()),
		Literal: lex.reader.Substring(), Line: lex.currentLine}
}

// Returns a token representing a string
func (lex *Lexer) readString() tokens.Token {

	lex.reader.StartNext()

	parts := make([]string, 0)

	for lex.reader.Peek() != '"' && !lex.reader.IsAtEnd() {

		switch lex.reader.Peek() {
		case '\\':
			parts = append(parts, lex.reader.Substring())
			lex.reader.Advance()
			switch lex.reader.Peek() {
			case 'a':
				parts = append(parts, "\a")
			case 'b':
				parts = append(parts, "\b")
			case 'f':
				parts = append(parts, "\f")
			case 'n':
				parts = append(parts, "\n")
			case 'r':
				parts = append(parts, "\r")
			case 't':
				parts = append(parts, "\t")
			case 'v':
				parts = append(parts, "\v")
			case '\\':
				parts = append(parts, "\\")
			case '"':
				parts = append(parts, "\"")
			default:
				parts = append(parts, string(lex.reader.Peek()))
			}
			lex.reader.Advance()
			lex.reader.StartNext()
		case '\n':
			lex.currentLine++
		}

		lex.reader.Advance()
	}

	//processed := lex.reader.Substring()
	parts = append(parts, lex.reader.Substring())
	processed := strings.Join(parts, "")

	if !lex.reader.IsAtEnd() {
		// Handling the closing "
		lex.reader.Advance()
	}

	return tokens.Token{Type: tokens.STRING, Literal: processed, Line: lex.currentLine}
}

// Returns true if the supplied rune is a character used in identifiers
func isIDChar(c rune) bool {
	return 'a' <= c && c <= 'z' || 'A' <= c && c <= 'Z' || c == '_'
}

// Returns true if the supplied rune is permitted in numbers. For now this is 0-9 and _.
func isDigitChar(c rune) bool {
	return '0' <= c && c <= '9' || c == '_'
}
