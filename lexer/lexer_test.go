package lexer

import (
	"gitlab.com/darkwyrm/mifflin/tokens"
	"testing"
)

func TestNextToken(t *testing.T) {
	input := `var five = 5;
var ten = 10;

var add = fn(x, y) {
  x + y;
};

var result = add(five, ten);
!-/*5;
**%;
5 < 10 > 5;
+=-=*=/=%=;

false and true;
true or false;

if (5 < 10) {
	return true;
} else {
	return false;
}

break;
continue;

10 == 10;
10 != 9;
"foobar"
"foo bar"
[1, 2];
{"foo": "bar"}
`

	tests := []struct {
		expectedType    tokens.TokenType
		expectedLiteral string
	}{
		{tokens.VAR, "var"},
		{tokens.IDENTIFIER, "five"},
		{tokens.EQ, "="},
		{tokens.INT, "5"},
		{tokens.SEMICOLON, ";"},
		{tokens.VAR, "var"},
		{tokens.IDENTIFIER, "ten"},
		{tokens.EQ, "="},
		{tokens.INT, "10"},
		{tokens.SEMICOLON, ";"},
		{tokens.VAR, "var"},
		{tokens.IDENTIFIER, "add"},
		{tokens.EQ, "="},
		{tokens.FN, "fn"},
		{tokens.LPAREN, "("},
		{tokens.IDENTIFIER, "x"},
		{tokens.COMMA, ","},
		{tokens.IDENTIFIER, "y"},
		{tokens.RPAREN, ")"},
		{tokens.LBRACE, "{"},
		{tokens.IDENTIFIER, "x"},
		{tokens.PLUS, "+"},
		{tokens.IDENTIFIER, "y"},
		{tokens.SEMICOLON, ";"},
		{tokens.RBRACE, "}"},
		{tokens.SEMICOLON, ";"},
		{tokens.VAR, "var"},
		{tokens.IDENTIFIER, "result"},
		{tokens.EQ, "="},
		{tokens.IDENTIFIER, "add"},
		{tokens.LPAREN, "("},
		{tokens.IDENTIFIER, "five"},
		{tokens.COMMA, ","},
		{tokens.IDENTIFIER, "ten"},
		{tokens.RPAREN, ")"},
		{tokens.SEMICOLON, ";"},
		{tokens.BANG, "!"},
		{tokens.MINUS, "-"},
		{tokens.SLASH, "/"},
		{tokens.STAR, "*"},
		{tokens.INT, "5"},
		{tokens.SEMICOLON, ";"},
		{tokens.DBLSTAR, "**"},
		{tokens.PERCENT, "%"},
		{tokens.SEMICOLON, ";"},
		{tokens.INT, "5"},
		{tokens.LT, "<"},
		{tokens.INT, "10"},
		{tokens.GT, ">"},
		{tokens.INT, "5"},
		{tokens.SEMICOLON, ";"},
		{tokens.PLUSEQ, "+="},
		{tokens.MINUSEQ, "-="},
		{tokens.STAREQ, "*="},
		{tokens.SLASHEQ, "/="},
		{tokens.PERCENTEQ, "%="},
		{tokens.SEMICOLON, ";"},
		{tokens.FALSE, "false"},
		{tokens.AND, "and"},
		{tokens.TRUE, "true"},
		{tokens.SEMICOLON, ";"},
		{tokens.TRUE, "true"},
		{tokens.OR, "or"},
		{tokens.FALSE, "false"},
		{tokens.SEMICOLON, ";"},
		{tokens.IF, "if"},
		{tokens.LPAREN, "("},
		{tokens.INT, "5"},
		{tokens.LT, "<"},
		{tokens.INT, "10"},
		{tokens.RPAREN, ")"},
		{tokens.LBRACE, "{"},
		{tokens.RETURN, "return"},
		{tokens.TRUE, "true"},
		{tokens.SEMICOLON, ";"},
		{tokens.RBRACE, "}"},
		{tokens.ELSE, "else"},
		{tokens.LBRACE, "{"},
		{tokens.RETURN, "return"},
		{tokens.FALSE, "false"},
		{tokens.SEMICOLON, ";"},
		{tokens.RBRACE, "}"},
		{tokens.BREAK, "break"},
		{tokens.SEMICOLON, ";"},
		{tokens.CONTINUE, "continue"},
		{tokens.SEMICOLON, ";"},
		{tokens.INT, "10"},
		{tokens.EQEQ, "=="},
		{tokens.INT, "10"},
		{tokens.SEMICOLON, ";"},
		{tokens.INT, "10"},
		{tokens.BANGEQ, "!="},
		{tokens.INT, "9"},
		{tokens.SEMICOLON, ";"},
		{tokens.STRING, "foobar"},
		{tokens.STRING, "foo bar"},
		{tokens.LBRACKET, "["},
		{tokens.INT, "1"},
		{tokens.COMMA, ","},
		{tokens.INT, "2"},
		{tokens.RBRACKET, "]"},
		{tokens.SEMICOLON, ";"},
		{tokens.LBRACE, "{"},
		{tokens.STRING, "foo"},
		{tokens.COLON, ":"},
		{tokens.STRING, "bar"},
		{tokens.RBRACE, "}"},
		{tokens.EOF, ""},
	}

	l := New(input)

	for i, tt := range tests {
		tok := l.NextToken()

		if tok.Type != tt.expectedType {
			t.Fatalf("tests[%d] - wrong token type on line %d . Expected %q, got %q",
				i, tok.Line, tokens.TokenTypeToString(tt.expectedType),
				tokens.TokenTypeToString(tok.Type))
		}

		if tok.Literal != tt.expectedLiteral {
			t.Fatalf("tests[%d] - literal wrong. expected %q, got %q",
				i, tt.expectedLiteral, tok.Literal)
		}
	}
}

func TestStringReading(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{`"foobar"`, "foobar"},
		{`"foo\bar"`, "foobar"},
		{`"foo\abar"`, "foo\abar"},
		{`"foo\bbar"`, "foo\bbar"},
		{`"foo\fbar"`, "foo\fbar"},
		{`"foo\nbar"`, "foo\nbar"},
		{`"foo\rbar"`, "foo\rbar"},
		{`"foo\tbar"`, "foo\tbar"},
		{`"foo\vbar"`, "foo\vbar"},
		{`"foo\\bar"`, "foo\\bar"},
		{`"foo\"abar"`, "foo\"bar"},
	}
	for _, tt := range tests {
		l := New(tt.input)
		tok := l.NextToken()
		if tok.Type != tokens.STRING {
			t.Fatalf("tests[%s]: Expected Identifier token type, got %q",
				tt.input, tokens.TokenTypeToString(tok.Type))
		}
	}
}
