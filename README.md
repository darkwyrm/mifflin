# Mifflin: A Utility Language for Systems Administration

## Description

Mifflin is a programming lanuguage intended make systems administration easier. It is inspired by Python, Kotlin, Go, and Rust and is released under the MIT license.

## Project Status

This is a fun personal project with a purpose. It's in early development, although getting it to the point of being useful is a priority. See below for a roadmap of features.

## Syntax and Structure

A Hello World program is hard to make any simpler:

```
println("Hello world!")
```
However, a more realistic, useful script could look more like this:

```
# Calculate a Fibonacci sequence
fn fib(a: int) -> int {
  if a < 2 {
    return a
  }
  return fib(a - 1) + fib(a - 2)
}

println(fib(5))
```

Mifflin is designed to feel familiar and, once it reaches release quality, it will also make a good first programming language. More information about structure and syntax will be forthcoming as the language develops.

## Project Roadmap

Mifflin is intended to be implemented in stages, and the first production-quality release made after
reaching the second. The first stage of implementation is complete and has the following features:

- primitive types: string, int, uint, float, bool
- variables
  - late binding for uninitialized variables
  - constants
- comments
- operators
  - Arithmetic Operators: +,-,*,/, %, **
  - Comparison Operators: <,>,<=,>=,==,!=
  - Assignment Operators: =, +=, -=, *=, /=
  - Logical Operators: !, or, and
- basic flow control
  - if
  - while
  - break
  - continue
- function definitions and calls
- stdlib:
  - print()/println()
  - prompt(msg) (console input)
  - exit()
  - panic()
- optional semicolons

This first stage is enough to do work, but won't be sufficient to use for actual production without a larger standard library. It will be an opportunity, however, to confirm basic language structure and work out any rough spots.

Implementation stage 2 will be capable of doing real work despite being fairly minimal. It will have
the following additional features:

- static typing
- classes
- the string type will become an object type
- lists
- dictionaries
- ranges
- for() loops
- shell syntax
- stdlib:
  - expand capabilities of existing built-ins
  - basic file I/O
  - basic networking
  - basic string manipulation
  - basic math

## Why Another Programming Language?

Simple: there aren't very many -- if any -- languages designed specifically for automating boring tasks on your computer or being a multipurpose tool for people who don't have a degree in Computer Science. Here is a quick summary of my issues with many popular languages for the task:

- Python
  - Pros: Easy for quick tasks, batteries included, readable syntax, mature
  - Cons: Package management is easy for users and hard for package creators. It doesn't scale well and a runtime is required. Dynamic typing leads to more runtime errors and more debugging. Although not a major issue for this problem domain, performance can be an issue at times. Lambda support is limited.
- Ruby
  - Pros: Like Python, it's easy to handle quick tasks and mature. It's also very powerful out of the box between the base language and the standard library.
  - Cons: Dynamic typing. Complexity -- the language syntax uses a lot of symbols that the user must know to perform basic tasks. This also contributes to poor readability, although not generally as bad as Perl. Also Ruby grants the ability to change the behavior of any built-in class (monkey patching).
- Bash
  - Pros: It comes with the operating system on *NIX, and pretty easy to add to Windows. The syntax is also very concise.
  - Debugging support is awful. No consistency: each tool is different and must be learned individually. Tool availability is inconsistent outside of a core set and installation is dependent on the operating system and/or the user. Poor readability.
- PowerShell
  - Pros: It comes with Windows and it's pretty easy to add to *NIX. Good vendor support.
  - Poor type consistency. Verbose and ugly syntax. Automatic variables which catch the unaware by surprise. Outside of a core set of commands, tools used are limited by operating system availability.
- Perl
  - Pros: Perl has been around for a long time and has a ton of modules available for it.
  - Cons: Terrible readability. It doesn't scale well and a runtime is required for use. Dynamic typing leads to more runtime errors and more debugging.

Mifflin is also designed to be easy to read, easy to learn, and generally boring to work with, enabling the user to focus on solving problems, not wrestling with the borrow checker or trying to understand generics, lambda calculus, or buffer overflows.
