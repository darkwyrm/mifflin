package parser_test

import (
	"testing"

	"gitlab.com/darkwyrm/mifflin/ast"
	"gitlab.com/darkwyrm/mifflin/lexer"
	"gitlab.com/darkwyrm/mifflin/parser"
)

func TestBreakStatements(t *testing.T) {
	input := `if x < y { break; };`
	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	if len(program.Statements) != 1 {
		t.Fatalf("program.Statements does not contain 1 statement, got %d",
			len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("program.Statements[0] is not an expression statement, got %T",
			program.Statements[0])
	}

	exp, ok := stmt.Expr.(*ast.IfExpression)
	if !ok {
		t.Fatalf("stmt.Expression is not ast.IfExpression, got %T",
			stmt.Expr)
	}

	if len(exp.Consequence.Statements) != 1 {
		t.Errorf("consequence is not 1 statement, got %d\n",
			len(exp.Consequence.Statements))
	}

	_, ok = exp.Consequence.Statements[0].(*ast.BreakStatement)
	if !ok {
		t.Fatalf("Statements[0] is not ast.BreakStatement, got %T",
			exp.Consequence.Statements[0])
	}
}

func TestContinueStatements(t *testing.T) {
	input := `if x < y { continue; };`
	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	if len(program.Statements) != 1 {
		t.Fatalf("program.Statements does not contain 1 statement, got %d",
			len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("program.Statements[0] is not an expression statement, got %T",
			program.Statements[0])
	}

	exp, ok := stmt.Expr.(*ast.IfExpression)
	if !ok {
		t.Fatalf("stmt.Expression is not ast.IfExpression, got %T",
			stmt.Expr)
	}

	if len(exp.Consequence.Statements) != 1 {
		t.Errorf("consequence is not 1 statement, got %d\n",
			len(exp.Consequence.Statements))
	}

	_, ok = exp.Consequence.Statements[0].(*ast.ContinueStatement)
	if !ok {
		t.Fatalf("Statements[0] is not ast.BreakStatement, got %T",
			exp.Consequence.Statements[0])
	}
}

func TestFnStatements(t *testing.T) {
	input := `fn main(x, y) { x + y; }`
	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	if len(program.Statements) != 1 {
		t.Fatalf("program.Statements does not contain 1 statement, got %d",
			len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.FunctionStatement)
	if !ok {
		t.Fatalf("program.Statements[0] is not a function statement, got %T",
			program.Statements[0])
	}

	if stmt.Name.Value != "main" {
		t.Fatalf("stmt.Name.Value not '%s', got %s", "main", stmt.Name.Value)
	}

	if len(stmt.Parameters) != 2 {
		t.Fatalf("Function statement has wrong parameter count. Expected 2, got %d\n",
			len(stmt.Parameters))
	}

	testLiteralExpression(t, stmt.Parameters[0], "x")
	testLiteralExpression(t, stmt.Parameters[1], "y")

	if len(stmt.Body.Statements) != 1 {
		t.Fatalf("function body doesn't have just 1 statement, got %d\n",
			len(stmt.Body.Statements))
	}

	bodyStmt, ok := stmt.Body.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Function body statement is not an expression statement, got %T",
			stmt.Body.Statements[0])
	}

	testInfixExpression(t, bodyStmt.Expr, "x", "+", "y")
}

func TestReturnStatements(t *testing.T) {
	tests := []struct {
		input         string
		expectedValue interface{}
	}{
		{"return 5;", 5},
		{"return true;", true},
		{"return foobar;", "foobar"},
	}

	for _, tt := range tests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		if len(program.Statements) != 1 {
			t.Fatalf("program.Statements does not contain 1 statements, got %d",
				len(program.Statements))
		}

		stmt := program.Statements[0]
		returnStmt, ok := stmt.(*ast.ReturnStatement)
		if !ok {
			t.Fatalf("stmt not *ast.returnStatement, got %T", stmt)
		}
		if returnStmt.TokenLiteral() != "return" {
			t.Fatalf("returnStmt.TokenLiteral not 'return', got %q",
				returnStmt.TokenLiteral())
		}
		if testLiteralExpression(t, returnStmt.ReturnValue, tt.expectedValue) {
			return
		}
	}
}

func TestVarStatements(t *testing.T) {
	tests := []struct {
		input              string
		expectedIdentifier string
		expectedValue      interface{}
	}{
		{"var x = 5;", "x", 5},
		{"var y = true;", "y", true},
		{"var foobar = y;", "foobar", "y"},
	}

	for _, tt := range tests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		if len(program.Statements) != 1 {
			t.Fatalf("program.Statements does not contain 1 statement, got %d",
				len(program.Statements))
		}

		stmt := program.Statements[0]
		if !testVarStatement(t, stmt, tt.expectedIdentifier) {
			return
		}

		val := stmt.(*ast.VarStatement).Value
		if !testLiteralExpression(t, val, tt.expectedValue) {
			return
		}
	}
}

func TestWhileStatements(t *testing.T) {
	input := `while x > y { println(x); y += 1; }`
	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	if len(program.Statements) != 1 {
		t.Fatalf("program.Statements does not contain 1 statement, got %d",
			len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.WhileStatement)
	if !ok {
		t.Fatalf("program.Statements[0] is not a while statement, got %T",
			program.Statements[0])
	}

	if !testInfixExpression(t, stmt.Condition, "x", ">", "y") {
		return
	}

	if len(stmt.Body.Statements) != 2 {
		t.Fatalf("while body should have 2 statements, got %d\n", len(stmt.Body.Statements))
	}

	_, ok = stmt.Body.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("First body statement is not an expression statement, got %T",
			stmt.Body.Statements[0])
	}

	body2, ok := stmt.Body.Statements[1].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Second body statement is not an expression statement, got %T",
			stmt.Body.Statements[1])
	}

	testInfixExpression(t, body2.Expr, "y", "+=", 1)
}

func TestOperatorPrecedenceParsing(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{
			"-a * b;",
			"((-a) * b); ",
		},
		{
			"!-a;",
			"(!(-a)); ",
		},
		{
			"a + b + c;",
			"((a + b) + c); ",
		},
		{
			"a + b - c;",
			"((a + b) - c); ",
		},
		{
			"a * b * c;",
			"((a * b) * c); ",
		},
		{
			"a * b / c;",
			"((a * b) / c); ",
		},
		{
			"a + b / c;",
			"(a + (b / c)); ",
		},
		{
			"a ** b % c;",
			"((a ** b) % c); ",
		},
		{
			"a + b * c + d / e - f;",
			"(((a + (b * c)) + (d / e)) - f); ",
		},
		{
			"3 + 4; -5 * 5;",
			"(3 + 4); ((-5) * 5); ",
		},
		{
			"5 > 4 == 3 < 4;",
			"((5 > 4) == (3 < 4)); ",
		},
		{
			"5 < 4 != 3 > 4;",
			"((5 < 4) != (3 > 4)); ",
		},
		{
			"3 + 4 * 5 == 3 * 1 + 4 * 5;",
			"((3 + (4 * 5)) == ((3 * 1) + (4 * 5))); ",
		},
		{
			"true;",
			"true; ",
		},
		{
			"false;",
			"false; ",
		},
		{
			"3 > 5 == false;",
			"((3 > 5) == false); ",
		},
		{
			"3 < 5 == true;",
			"((3 < 5) == true); ",
		},
		{
			"1 + (2 + 3) + 4;",
			"((1 + (2 + 3)) + 4); ",
		},
		{
			"(5 + 5) * 2;",
			"((5 + 5) * 2); ",
		},
		{
			"2 / (5 + 5);",
			"(2 / (5 + 5)); ",
		},
		{
			"(5 + 5) * 2 * (5 + 5);",
			"(((5 + 5) * 2) * (5 + 5)); ",
		},
		{
			"-(5 + 5);",
			"(-(5 + 5)); ",
		},
		{
			"!(true == true);",
			"(!(true == true)); ",
		},
		{
			"a + add(b * c) + d;",
			"((a + add((b * c))) + d); ",
		},
		{
			"add(a, b, 1, 2 * 3, 4 + 5, add(6, 7 * 8));",
			"add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8))); ",
		},
		{
			"add(a + b + c * d / f + g);",
			"add((((a + b) + ((c * d) / f)) + g)); ",
		},
		{
			"a * [1, 2, 3, 4][b * c] * d;",
			"((a * ([1, 2, 3, 4][(b * c)])) * d); ",
		},
		{
			"add(a * b[2], b[1], 2 * [1, 2][1]);",
			"add((a * (b[2])), (b[1]), (2 * ([1, 2][1]))); ",
		},
	}

	for _, tt := range tests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		actual := program.String()
		if actual != tt.expected {
			t.Errorf("expected %q, got %q", tt.expected, actual)
		}
	}
}

func TestSemicolonInference(t *testing.T) {
	semiTests := []struct {
		input string
		count int
	}{
		// Uninitialized variable declarations
		{"var a\n !5;", 2},
		{"const b\n println(b);", 2},
		{"if x < y { const b };", 1},
		{"var c", 1},

		// Initialized variable declarations
		{"var a = 1\n var b", 2},
		{"var a = 2\n println(b);", 2},
		{"var a = 2 +\n 5", 1},
		{"if x < y { const b = 5 };", 1},

		// Return statements
		{"return 5\n var b", 2},
		{"return 5\n - b", 1},
		{"if x < y { return 5 };", 1},

		// Expression statements
		{"if x < y { 5 } else { 10 }\n * 5", 1},
		{"if x < y { const b = 5 }\n return 5", 2},
	}

	for _, tt := range semiTests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		if len(program.Statements) != tt.count {
			t.Fatalf("Statement count error for program `%s`. Expected %d, got %d ",
				tt.input, tt.count, len(program.Statements))
		}
		//fmt.Printf(program.String())
	}
}
