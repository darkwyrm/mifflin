package parser

import "gitlab.com/darkwyrm/mifflin/tokens"

type Precedence int

const (
	_ Precedence = iota
	LOWEST
	ASSIGNMENT
	AND
	OR
	EQUALITY
	COMPARISON
	SUM
	PRODUCT
	PREFIX
	POWER
	CALL
	INDEX
)

var precedenceMap = map[tokens.TokenType]Precedence{
	tokens.EQ:        ASSIGNMENT,
	tokens.PLUSEQ:    ASSIGNMENT,
	tokens.MINUSEQ:   ASSIGNMENT,
	tokens.STAREQ:    ASSIGNMENT,
	tokens.SLASHEQ:   ASSIGNMENT,
	tokens.PERCENTEQ: ASSIGNMENT,

	tokens.AND: AND,
	tokens.OR:  OR,

	tokens.EQEQ:   EQUALITY,
	tokens.BANGEQ: EQUALITY,

	tokens.LT:   COMPARISON,
	tokens.LTEQ: COMPARISON,
	tokens.GT:   COMPARISON,
	tokens.GTEQ: COMPARISON,

	tokens.PLUS:    SUM,
	tokens.MINUS:   SUM,
	tokens.STAR:    PRODUCT,
	tokens.SLASH:   PRODUCT,
	tokens.PERCENT: PRODUCT,
	tokens.DBLSTAR: POWER,

	tokens.LPAREN:   CALL,
	tokens.LBRACKET: INDEX,
}
