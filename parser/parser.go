package parser

import (
	"fmt"
	"strconv"

	"gitlab.com/darkwyrm/mifflin/ast"
	"gitlab.com/darkwyrm/mifflin/lexer"
	"gitlab.com/darkwyrm/mifflin/tokens"
)

type (
	prefixFn func() ast.Expression
	infixFn  func(ast.Expression) ast.Expression
)

// The Parser type handles turning a list of tokens into a list of statements
type Parser struct {
	lex *lexer.Lexer

	Previous tokens.Token
	Current  tokens.Token
	Next     tokens.Token
	errors   []string

	prefixFnMap map[tokens.TokenType]prefixFn
	infixFnMap  map[tokens.TokenType]infixFn
}

func New(l *lexer.Lexer) *Parser {
	p := &Parser{lex: l, errors: []string{}}
	p.nextToken()
	p.nextToken()

	p.prefixFnMap = make(map[tokens.TokenType]prefixFn)
	p.registerPrefix(tokens.BANG, p.parsePrefixExpression)
	p.registerPrefix(tokens.FALSE, p.parseBooleanLiteral)
	p.registerPrefix(tokens.FLOAT, p.parseFloatLiteral)
	p.registerPrefix(tokens.FN, p.parseFunctionLiteral)
	p.registerPrefix(tokens.IDENTIFIER, p.parseIdentifier)
	p.registerPrefix(tokens.IF, p.parseIfExpression)
	p.registerPrefix(tokens.INT, p.parseIntegerLiteral)
	p.registerPrefix(tokens.LBRACKET, p.parseListLiteral)
	p.registerPrefix(tokens.LPAREN, p.parseGroupedExpression)
	p.registerPrefix(tokens.MINUS, p.parsePrefixExpression)
	p.registerPrefix(tokens.NONE, p.parseNoneLiteral)
	p.registerPrefix(tokens.STRING, p.parseStringLiteral)
	p.registerPrefix(tokens.TRUE, p.parseBooleanLiteral)
	p.registerPrefix(tokens.UINT, p.parseUIntegerLiteral)

	p.infixFnMap = make(map[tokens.TokenType]infixFn)
	p.registerInfix(tokens.AND, p.parseInfixExpression)
	p.registerInfix(tokens.OR, p.parseInfixExpression)
	p.registerInfix(tokens.PLUS, p.parseInfixExpression)
	p.registerInfix(tokens.PLUSEQ, p.parseInfixExpression)
	p.registerInfix(tokens.MINUS, p.parseInfixExpression)
	p.registerInfix(tokens.MINUSEQ, p.parseInfixExpression)
	p.registerInfix(tokens.STAR, p.parseInfixExpression)
	p.registerInfix(tokens.STAREQ, p.parseInfixExpression)
	p.registerInfix(tokens.SLASH, p.parseInfixExpression)
	p.registerInfix(tokens.SLASHEQ, p.parseInfixExpression)
	p.registerInfix(tokens.DBLSTAR, p.parseInfixExpression)
	p.registerInfix(tokens.PERCENT, p.parseInfixExpression)
	p.registerInfix(tokens.PERCENTEQ, p.parseInfixExpression)
	p.registerInfix(tokens.EQ, p.parseInfixExpression)
	p.registerInfix(tokens.EQEQ, p.parseInfixExpression)
	p.registerInfix(tokens.BANGEQ, p.parseInfixExpression)
	p.registerInfix(tokens.LT, p.parseInfixExpression)
	p.registerInfix(tokens.LTEQ, p.parseInfixExpression)
	p.registerInfix(tokens.GT, p.parseInfixExpression)
	p.registerInfix(tokens.GTEQ, p.parseInfixExpression)
	p.registerInfix(tokens.LPAREN, p.parseCallExpression)
	p.registerInfix(tokens.LBRACKET, p.parseIndexExpression)

	return p
}

func (p *Parser) nextToken() {
	p.Previous = p.Current
	p.Current = p.Next
	p.Next = p.lex.NextToken()
}

func (p *Parser) previousTokenIs(tokType tokens.TokenType) bool {
	return p.Previous.Type == tokType
}

func (p *Parser) currentTokenIs(tokType tokens.TokenType) bool {
	return p.Current.Type == tokType
}

func (p *Parser) nextTokenIs(tokType tokens.TokenType) bool {
	return p.Next.Type == tokType
}

func (p *Parser) expectNext(tokType tokens.TokenType) bool {
	if p.nextTokenIs(tokType) {
		p.nextToken()
		return true
	}
	p.nextError(tokType)
	return false
}

func (p *Parser) currentPrecedence() Precedence {
	if out, ok := precedenceMap[p.Current.Type]; ok {
		return out
	}
	return LOWEST
}

func (p *Parser) nextPrecedence() Precedence {
	if out, ok := precedenceMap[p.Next.Type]; ok {
		return out
	}
	return LOWEST
}

func (p *Parser) Errors() []string {
	return p.errors
}

func (p *Parser) addError(msg string, args ...string) {
	if args != nil {
		p.errors = append(p.errors, fmt.Sprintf(msg, args))
	} else {
		p.errors = append(p.errors, msg)
	}
}

func (p *Parser) nextError(tokType tokens.TokenType) {
	msg := fmt.Sprintf("Expected %s, but got %s", tokens.TokenTypeToString(tokType),
		tokens.TokenTypeToString(p.Next.Type))
	p.errors = append(p.errors, msg)
}

// nextIsTerminator returns true if the next token is one which can terminate a statement.
func (p *Parser) nextIsTerminator() bool {
	switch p.Next.Type {
	case tokens.SEMICOLON, tokens.RBRACE, tokens.EOF:
		return true
	default:
		return false
	}
}

// nextStartsNew returns true if the next token starts a new next statement. By not just checking
// for semicolons, we are able to make semicolons optional.
func (p *Parser) nextStartsNew() bool {
	// If the next token is on the same line, it definitely doesn't start a new statement.
	if p.Next.Line == p.Current.Line {
		return false
	}

	// Keywords sometimes start a statement: class, const, return, var, and while
	switch p.Next.Type {
	case tokens.CLASS, tokens.CONST, tokens.RETURN, tokens.VAR, tokens.WHILE, tokens.IDENTIFIER:
		return true
	default:
		return false
	}
}

func (p *Parser) ParseProgram() *ast.Program {
	program := &ast.Program{}
	program.Statements = []ast.Statement{}

	for p.Current.Type != tokens.EOF {
		stmt := p.parseStatement()
		if stmt != nil {
			program.Statements = append(program.Statements, stmt)
		}
		p.nextToken()
	}

	return program
}

func (p *Parser) parseStatement() ast.Statement {
	switch p.Current.Type {
	// Ignore empty statements
	case tokens.SEMICOLON:
		return nil
	case tokens.BREAK:
		return p.parseBreakStatement()
	case tokens.CONTINUE:
		return p.parseContinueStatement()
	case tokens.CONST:
		return p.parseVarStatement(true)
	case tokens.FN:
		return p.parseFnStatement()
	case tokens.RETURN:
		return p.parseReturnStatement()
	case tokens.VAR:
		return p.parseVarStatement(false)
	case tokens.WHILE:
		return p.parseWhileStatement()
	default:
		return p.parseExpressionStatement()
	}
}

func (p *Parser) parseBreakStatement() *ast.BreakStatement {
	if p.nextTokenIs(tokens.SEMICOLON) {
		p.nextToken()
	}
	return &ast.BreakStatement{Token: p.Current}
}

func (p *Parser) parseContinueStatement() *ast.ContinueStatement {
	if p.nextTokenIs(tokens.SEMICOLON) {
		p.nextToken()
	}
	return &ast.ContinueStatement{Token: p.Current}
}

func (p *Parser) parseBlockStatement() *ast.BlockStatement {
	block := &ast.BlockStatement{Token: p.Current}
	block.Statements = []ast.Statement{}

	p.nextToken()

	for !p.currentTokenIs(tokens.RBRACE) {
		stmt := p.parseStatement()
		if stmt != nil {
			block.Statements = append(block.Statements, stmt)
		}
		p.nextToken()
	}
	return block
}

func (p *Parser) parseExpressionStatement() ast.Statement {
	stmt := &ast.ExpressionStatement{Token: p.Current}

	stmt.Expr = p.parseExpression(LOWEST)
	if p.nextIsTerminator() || p.nextStartsNew() {
		if p.nextTokenIs(tokens.SEMICOLON) {
			p.nextToken()
		}

		return stmt
	}
	if stmt.Expr != nil {
		p.addError("Semicolon missing from expression statement %s", stmt.Expr.String())
	} else {
		p.addError("Semicolon missing from expression statement %s", stmt.String())
	}

	return nil
}

func (p *Parser) parseFnStatement() *ast.FunctionStatement {
	stmt := &ast.FunctionStatement{Token: p.Current}

	if !p.expectNext(tokens.IDENTIFIER) {
		return nil
	}

	stmt.Name = &ast.Identifier{Token: p.Current, Value: p.Current.Literal}

	if !p.expectNext(tokens.LPAREN) {
		return nil
	}

	stmt.Parameters = p.parseFunctionParameters()

	if !p.expectNext(tokens.LBRACE) {
		return nil
	}

	stmt.Body = p.parseBlockStatement()

	return stmt
}

func (p *Parser) parseReturnStatement() ast.Statement {
	stmt := &ast.ReturnStatement{Token: p.Current}

	p.nextToken()

	stmt.ReturnValue = p.parseExpression(LOWEST)
	if p.nextIsTerminator() || p.nextStartsNew() {
		if p.nextTokenIs(tokens.SEMICOLON) {
			p.nextToken()
		}

		return stmt
	}
	p.addError("Semicolon missing from return statement")
	return nil
}

func (p *Parser) parseVarStatement(isConst bool) ast.Statement {
	startToken := p.Current

	if !p.expectNext(tokens.IDENTIFIER) {
		return nil
	}

	name := &ast.Identifier{Token: p.Current, Value: p.Current.Literal}

	if p.nextIsTerminator() || !p.nextTokenIs(tokens.EQ) && p.Next.Line > p.Current.Line {
		if p.nextTokenIs(tokens.SEMICOLON) {
			p.nextToken()
		}

		if isConst {
			return &ast.ConstStatement{Token: startToken, Name: name, Value: &ast.UndefinedLiteral{}}
		} else {
			return &ast.VarStatement{Token: startToken, Name: name, Value: &ast.UndefinedLiteral{}}
		}
	}

	if !p.expectNext(tokens.EQ) {
		return nil
	}
	p.nextToken()

	value := p.parseExpression(LOWEST)

	if p.nextIsTerminator() || p.nextStartsNew() {
		if p.nextTokenIs(tokens.SEMICOLON) {
			p.nextToken()
		}

		if isConst {
			return &ast.ConstStatement{Token: startToken, Name: name, Value: value}
		} else {
			return &ast.VarStatement{Token: startToken, Name: name, Value: value}
		}
	}

	if isConst {
		p.addError("Semicolon missing from const statement")
	} else {
		p.addError("Semicolon missing from var statement")
	}
	return nil
}

func (p *Parser) parseWhileStatement() *ast.WhileStatement {
	wstmt := &ast.WhileStatement{Token: p.Current}
	wstmt.Body = &ast.BlockStatement{}
	wstmt.Body.Statements = []ast.Statement{}

	p.nextToken()

	wstmt.Condition = p.parseExpression(LOWEST)

	if !p.expectNext(tokens.LBRACE) {
		return nil
	}

	wstmt.Body = p.parseBlockStatement()
	return wstmt
}

func (p *Parser) parseExpression(precedence Precedence) ast.Expression {
	prefix := p.prefixFnMap[p.Current.Type]
	if prefix == nil {
		msg := fmt.Sprintf("Prefix parser function for %s not found",
			tokens.TokenTypeToString(p.Current.Type))
		p.errors = append(p.errors, msg)
		return nil
	}
	left := prefix()

	for !p.nextTokenIs(tokens.SEMICOLON) && precedence < p.nextPrecedence() {
		infix := p.infixFnMap[p.Next.Type]
		if infix == nil {
			return left
		}
		p.nextToken()
		left = infix(left)
	}

	return left
}

func (p *Parser) parseGroupedExpression() ast.Expression {
	p.nextToken()

	expr := p.parseExpression(LOWEST)
	if !p.expectNext(tokens.RPAREN) {
		return nil
	}

	return expr
}

func (p *Parser) parseCallExpression(function ast.Expression) ast.Expression {
	exp := &ast.CallExpression{Token: p.Current, Function: function}
	exp.Arguments = p.parseExpressionList(tokens.RPAREN)
	return exp
}

func (p *Parser) parseExpressionList(end tokens.TokenType) []ast.Expression {
	list := make([]ast.Expression, 0)

	if p.nextTokenIs(end) {
		p.nextToken()
		return list
	}

	p.nextToken()
	list = append(list, p.parseExpression(LOWEST))

	for p.nextTokenIs(tokens.COMMA) {
		p.nextToken()
		p.nextToken()
		list = append(list, p.parseExpression(LOWEST))
	}

	if !p.expectNext(end) {
		return nil
	}

	return list
}

func (p *Parser) parseIndexExpression(left ast.Expression) ast.Expression {
	exp := &ast.IndexExpression{Token: p.Current, Left: left}

	p.nextToken()
	exp.Index = p.parseExpression(LOWEST)

	if !p.expectNext(tokens.RBRACKET) {
		return nil
	}

	return exp
}

func (p *Parser) parseHashLiteral() ast.Expression {
	hash := &ast.HashLiteral{Token: p.Current}
	hash.Pairs = make(map[ast.Expression]ast.Expression)

	for !p.nextTokenIs(tokens.RBRACE) {
		p.nextToken()
		key := p.parseExpression(LOWEST)

		if !p.expectNext(tokens.COLON) {
			return nil
		}

		p.nextToken()
		value := p.parseExpression(LOWEST)

		hash.Pairs[key] = value

		if !p.nextTokenIs(tokens.RBRACE) && !p.expectNext(tokens.COMMA) {
			return nil
		}
	}

	if !p.expectNext(tokens.RBRACE) {
		return nil
	}

	return hash
}

func (p *Parser) parseIfExpression() ast.Expression {
	expr := &ast.IfExpression{Token: p.Current}

	p.nextToken()
	expr.Condition = p.parseExpression(LOWEST)

	if !p.expectNext(tokens.LBRACE) {
		return nil
	}

	expr.Consequence = p.parseBlockStatement()

	if p.nextTokenIs(tokens.ELSE) {
		p.nextToken()

		if !p.expectNext(tokens.LBRACE) {
			return nil
		}

		expr.Alternative = p.parseBlockStatement()
	}

	return expr
}

func (p *Parser) parseInfixExpression(left ast.Expression) ast.Expression {
	expr := &ast.InfixExpression{
		Token:    p.Current,
		Operator: p.Current.Literal,
		Left:     left,
	}

	precedence := p.currentPrecedence()
	p.nextToken()
	expr.Right = p.parseExpression(precedence)

	return expr
}

func (p *Parser) parsePrefixExpression() ast.Expression {
	expr := &ast.PrefixExpression{
		Token:    p.Current,
		Operator: p.Current.Literal,
	}
	p.nextToken()

	expr.Right = p.parseExpression(PREFIX)

	return expr
}

func (p *Parser) parseIdentifier() ast.Expression {
	return &ast.Identifier{Token: p.Current, Value: p.Current.Literal}
}

func (p *Parser) parseBooleanLiteral() ast.Expression {
	return &ast.BooleanLiteral{Token: p.Current, Value: p.currentTokenIs(tokens.TRUE)}
}

func (p *Parser) parseFloatLiteral() ast.Expression {
	lit := &ast.FloatLiteral{Token: p.Current}

	value, err := strconv.ParseFloat(p.Current.Literal, 64)
	if err != nil {
		msg := fmt.Sprintf("Could not parse %q as float", p.Current.Literal)
		p.errors = append(p.errors, msg)
		return nil
	}

	lit.Value = value
	return lit
}

func (p *Parser) parseFunctionLiteral() ast.Expression {
	lit := &ast.FunctionLiteral{Token: p.Current}

	if !p.expectNext(tokens.LPAREN) {
		return nil
	}

	lit.Parameters = p.parseFunctionParameters()

	if !p.expectNext(tokens.LBRACE) {
		return nil
	}

	lit.Body = p.parseBlockStatement()

	return lit
}

func (p *Parser) parseFunctionParameters() []*ast.Identifier {
	identifiers := make([]*ast.Identifier, 0)

	if p.nextTokenIs(tokens.RPAREN) {
		p.nextToken()
		return identifiers
	}

	p.nextToken()

	ident := &ast.Identifier{Token: p.Current, Value: p.Current.Literal}
	identifiers = append(identifiers, ident)

	for p.nextTokenIs(tokens.COMMA) {
		p.nextToken()
		p.nextToken()
		ident := &ast.Identifier{Token: p.Current, Value: p.Current.Literal}
		identifiers = append(identifiers, ident)
	}

	if !p.expectNext(tokens.RPAREN) {
		return nil
	}

	return identifiers
}

func (p *Parser) parseIntegerLiteral() ast.Expression {
	lit := &ast.IntegerLiteral{Token: p.Current}

	value, err := strconv.ParseInt(p.Current.Literal, 0, 64)
	if err != nil {
		msg := fmt.Sprintf("Could not parse %q as integer", p.Current.Literal)
		p.errors = append(p.errors, msg)
		return nil
	}

	lit.Value = value
	return lit
}

func (p *Parser) parseListLiteral() ast.Expression {
	lit := &ast.ListLiteral{Token: p.Current}
	lit.Elements = p.parseExpressionList(tokens.RBRACKET)
	return lit
}

func (p *Parser) parseNoneLiteral() ast.Expression {
	return &ast.NoneLiteral{}
}

func (p *Parser) parseStringLiteral() ast.Expression {
	return &ast.StringLiteral{Token: p.Current, Value: p.Current.Literal}
}

func (p *Parser) parseUIntegerLiteral() ast.Expression {
	lit := &ast.UIntegerLiteral{Token: p.Current}

	value, err := strconv.ParseUint(p.Current.Literal, 0, 64)
	if err != nil {
		msg := fmt.Sprintf("Could not parse %q as unsigned integer", p.Current.Literal)
		p.errors = append(p.errors, msg)
		return nil
	}

	lit.Value = value
	return lit
}

func (p *Parser) registerPrefix(tokType tokens.TokenType, fn prefixFn) {
	p.prefixFnMap[tokType] = fn
}

func (p *Parser) registerInfix(tokType tokens.TokenType, fn infixFn) {
	p.infixFnMap[tokType] = fn
}
