package parser_test

import (
	"gitlab.com/darkwyrm/mifflin/ast"
	"gitlab.com/darkwyrm/mifflin/lexer"
	"gitlab.com/darkwyrm/mifflin/parser"
	"testing"
)

func TestParsingEmptyArrayLiterals(t *testing.T) {
	input := "[];"

	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	array, ok := stmt.Expr.(*ast.ListLiteral)
	if !ok {
		t.Fatalf("exp not ast.ListLiteral, got %T", stmt.Expr)
	}

	if len(array.Elements) != 0 {
		t.Errorf("len(array.Elements) not 0, got %d", len(array.Elements))
	}
}

func TestParsingArrayLiterals(t *testing.T) {
	input := "[1, 2 * 2, 3 + 3];"

	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	array, ok := stmt.Expr.(*ast.ListLiteral)
	if !ok {
		t.Fatalf("exp not ast.ListLiteral, got %T", stmt.Expr)
	}

	if len(array.Elements) != 3 {
		t.Fatalf("len(array.Elements) not 3, got %d", len(array.Elements))
	}

	testIntegerLiteral(t, array.Elements[0], 1)
	testInfixExpression(t, array.Elements[1], 2, "*", 2)
	testInfixExpression(t, array.Elements[2], 3, "+", 3)
}

func TestParsingIndexExpressions(t *testing.T) {
	input := "myList[1 + 1];"

	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	indexExp, ok := stmt.Expr.(*ast.IndexExpression)
	if !ok {
		t.Fatalf("exp not *ast.IndexExpression, got %T", stmt.Expr)
	}

	if !testIdentifier(t, indexExp.Left, "myList") {
		return
	}

	if !testInfixExpression(t, indexExp.Index, 1, "+", 1) {
		return
	}
}

func TestBooleanExpression(t *testing.T) {
	tests := []struct {
		input           string
		expectedBoolean bool
	}{
		{"true;", true},
		{"false;", false},
	}

	for _, tt := range tests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		if len(program.Statements) != 1 {
			t.Fatalf("program.Statements does not contain 1 statement, got %d",
				len(program.Statements))
		}

		stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
		if !ok {
			t.Fatalf("program.Statements[0] is not ast.ExpressionStatement, got %T",
				program.Statements[0])
		}

		boolean, ok := stmt.Expr.(*ast.BooleanLiteral)
		if !ok {
			t.Fatalf("expression is not Boolean, got %T", stmt.Expr)
		}
		if boolean.Value != tt.expectedBoolean {
			t.Errorf("boolean value not %t, got %t", tt.expectedBoolean, boolean.Value)
		}
	}
}

func TestStringLiteralExpression(t *testing.T) {
	input := `"hello world";`

	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	stmt := program.Statements[0].(*ast.ExpressionStatement)
	literal, ok := stmt.Expr.(*ast.StringLiteral)
	if !ok {
		t.Fatalf("exp not *ast.StringLiteral, got %T", stmt.Expr)
	}

	if literal.Value != "hello world" {
		t.Errorf("literal.Value not %q, got %q", "hello world", literal.Value)
	}
}

func TestCallExpressionParsing(t *testing.T) {
	input := "add(1, 2 * 3, 4 + 5);"

	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	if len(program.Statements) != 1 {
		t.Fatalf("statement list does not contain just 1 statement, got %d\n",
			len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("stmt is not an expression statement, got %T", program.Statements[0])
	}

	exp, ok := stmt.Expr.(*ast.CallExpression)
	if !ok {
		t.Fatalf("Expression statement is not ast.CallExpression, got %T", stmt.Expr)
	}

	if !testIdentifier(t, exp.Function, "add") {
		return
	}

	if len(exp.Arguments) != 3 {
		t.Fatalf("Expected 3 arguments, got %d", len(exp.Arguments))
	}

	testLiteralExpression(t, exp.Arguments[0], 1)
	testInfixExpression(t, exp.Arguments[1], 2, "*", 3)
	testInfixExpression(t, exp.Arguments[2], 4, "+", 5)
}

func TestCallExpressionParameterParsing(t *testing.T) {
	tests := []struct {
		input         string
		expectedIdent string
		expectedArgs  []string
	}{
		{
			input:         "add();",
			expectedIdent: "add",
			expectedArgs:  []string{},
		},
		{
			input:         "add(1);",
			expectedIdent: "add",
			expectedArgs:  []string{"1"},
		},
		{
			input:         "add(1, 2 * 3, 4 + 5);",
			expectedIdent: "add",
			expectedArgs:  []string{"1", "(2 * 3)", "(4 + 5)"},
		},
	}

	for _, tt := range tests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		stmt := program.Statements[0].(*ast.ExpressionStatement)
		exp, ok := stmt.Expr.(*ast.CallExpression)
		if !ok {
			t.Fatalf("Expression statement is not ast.CallExpression, got %T", stmt.Expr)
		}

		if !testIdentifier(t, exp.Function, tt.expectedIdent) {
			return
		}

		if len(exp.Arguments) != len(tt.expectedArgs) {
			t.Fatalf("Argument count mismatch. Expected %d, got %d",
				len(tt.expectedArgs), len(exp.Arguments))
		}

		for i, arg := range tt.expectedArgs {
			if exp.Arguments[i].String() != arg {
				t.Errorf("Argument %d value mismatch. Expected %q, got %q", i,
					arg, exp.Arguments[i].String())
			}
		}
	}
}

func TestFunctionLiterals(t *testing.T) {
	input := `var foo = fn(x, y) { x + y; };`
	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	if len(program.Statements) != 1 {
		t.Fatalf("program.Statements does not contain 1 statement, got %d",
			len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.VarStatement)
	if !ok {
		t.Fatalf("program.Statements[0] is not an expression statement, got %T",
			program.Statements[0])
	}

	function, ok := stmt.Value.(*ast.FunctionLiteral)
	if !ok {
		t.Fatalf("stmt.Value is not a function literal, got %T", stmt.Value)
	}

	if len(function.Parameters) != 2 {
		t.Fatalf("Function literal has wrong parameter count. Expected 2, got %d\n",
			len(function.Parameters))
	}

	testLiteralExpression(t, function.Parameters[0], "x")
	testLiteralExpression(t, function.Parameters[1], "y")

	if len(function.Body.Statements) != 1 {
		t.Fatalf("function body doesn't have just 1 statement, got %d\n",
			len(function.Body.Statements))
	}

	bodyStmt, ok := function.Body.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Function body statement is not an expression statement, got %T",
			function.Body.Statements[0])
	}

	testInfixExpression(t, bodyStmt.Expr, "x", "+", "y")
}

func TestFunctionParameters(t *testing.T) {
	tests := []struct {
		input          string
		expectedParams []string
	}{
		{input: "var foo = fn() {};", expectedParams: []string{}},
		{input: "var foo = fn(x) {};", expectedParams: []string{"x"}},
		{input: "var foo = fn(x, y, z) {};", expectedParams: []string{"x", "y", "z"}},
	}

	for _, tt := range tests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		stmt := program.Statements[0].(*ast.VarStatement)
		function := stmt.Value.(*ast.FunctionLiteral)

		if len(function.Parameters) != len(tt.expectedParams) {
			t.Errorf("length parameters wrong. want %d, got %d\n",
				len(tt.expectedParams), len(function.Parameters))
		}

		for i, ident := range tt.expectedParams {
			testLiteralExpression(t, function.Parameters[i], ident)
		}
	}
}

func TestIdentifierExpressions(t *testing.T) {
	tests := []struct {
		input              string
		expectedIdentifier string
		expectedValue      interface{}
	}{
		{"var x = 5;", "x", 5},
		{"var y = 3145;", "y", 3145},
		{"var foobar = y;", "foobar", "y"},
	}

	for _, tt := range tests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		if len(program.Statements) != 1 {
			t.Fatalf("program.Statements does not contain 1 statement, got %d",
				len(program.Statements))
		}

		stmt := program.Statements[0]
		if !testVarStatement(t, stmt, tt.expectedIdentifier) {
			return
		}

		val := stmt.(*ast.VarStatement).Value
		if !testLiteralExpression(t, val, tt.expectedValue) {
			return
		}
	}
}

func TestIfExpressions(t *testing.T) {
	tests := []struct {
		input         string
		expectedValue string
	}{
		{`if true { "a"; };`, "a"},
		{`if false { "b"; };`, "b"},
	}

	for _, tt := range tests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		if len(program.Statements) != 1 {
			t.Fatalf("program.Statements does not contain 1 statement, got %d",
				len(program.Statements))
		}

		stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
		if !ok {
			t.Fatalf("program.Statements[0] is not ast.ExpressionStatement, got %T",
				program.Statements[0])
		}

		exp, ok := stmt.Expr.(*ast.IfExpression)
		if !ok {
			t.Fatalf("stmt.Expression is not ast.IfExpression, got %T",
				stmt.Expr)
		}

		if len(exp.Consequence.Statements) != 1 {
			t.Errorf("consequence is not 1 statement, got %d\n",
				len(exp.Consequence.Statements))
		}

		consequence, ok := exp.Consequence.Statements[0].(*ast.ExpressionStatement)
		if !ok {
			t.Fatalf("Statements[0] is not ast.ExpressionStatement, got %T",
				exp.Consequence.Statements[0])
		}

		if !testStringLiteral(t, consequence.Expr, tt.expectedValue) {
			return
		}
	}
}

func TestIfElseExpression(t *testing.T) {
	input := `if (x < y) { x; } else { y; };`

	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	if len(program.Statements) != 1 {
		t.Fatalf("program.Body does not contain %d statements, got %d\n",
			1, len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("program.Statements[0] is not ast.ExpressionStatement, got %T",
			program.Statements[0])
	}

	exp, ok := stmt.Expr.(*ast.IfExpression)
	if !ok {
		t.Fatalf("stmt.Expression is not ast.IfExpression, got %T", stmt.Expr)
	}

	if !testInfixExpression(t, exp.Condition, "x", "<", "y") {
		return
	}

	if len(exp.Consequence.Statements) != 1 {
		t.Errorf("consequence is not 1 statements, got %d\n",
			len(exp.Consequence.Statements))
	}

	consequence, ok := exp.Consequence.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Statements[0] is not ast.ExpressionStatement, got %T",
			exp.Consequence.Statements[0])
	}

	if !testIdentifier(t, consequence.Expr, "x") {
		return
	}

	if len(exp.Alternative.Statements) != 1 {
		t.Errorf("exp.Alternative.Statements does not contain 1 statements, got %d\n",
			len(exp.Alternative.Statements))
	}

	alternative, ok := exp.Alternative.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Statements[0] is not ast.ExpressionStatement, got %T",
			exp.Alternative.Statements[0])
	}

	if !testIdentifier(t, alternative.Expr, "y") {
		return
	}
}

func TestInfixExpressions(t *testing.T) {
	infixTests := []struct {
		input      string
		leftValue  interface{}
		operator   string
		rightValue interface{}
	}{
		{"5 + 5;", 5, "+", 5},
		{"5 - 5;", 5, "-", 5},
		{"5 * 5;", 5, "*", 5},
		{"5 / 5;", 5, "/", 5},
		{"5 ** 5;", 5, "**", 5},
		{"5 % 5;", 5, "%", 5},
		{"5 > 5;", 5, ">", 5},
		{"5 < 5;", 5, "<", 5},
		{"5 == 5;", 5, "==", 5},
		{"5 != 5;", 5, "!=", 5},
		{"foobar + barfoo;", "foobar", "+", "barfoo"},
		{"foobar - barfoo;", "foobar", "-", "barfoo"},
		{"foobar * barfoo;", "foobar", "*", "barfoo"},
		{"foobar / barfoo;", "foobar", "/", "barfoo"},
		{"foobar > barfoo;", "foobar", ">", "barfoo"},
		{"foobar < barfoo;", "foobar", "<", "barfoo"},
		{"foobar == barfoo;", "foobar", "==", "barfoo"},
		{"foobar != barfoo;", "foobar", "!=", "barfoo"},
		{"true == true;", true, "==", true},
		{"true != false;", true, "!=", false},
		{"false == false;", false, "==", false},
		{"true and true;", true, "and", true},
		{"false or true;", false, "or", true},
	}
	for _, tt := range infixTests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		if len(program.Statements) != 1 {
			t.Fatalf("Expected 1 statement, got %d", len(program.Statements))
		}

		stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
		if !ok {
			t.Fatalf("program.Statements[0] is not ast.ExpressionStatement, got %T",
				program.Statements[0])
		}

		if !testInfixExpression(t, stmt.Expr, tt.leftValue, tt.operator, tt.rightValue) {
			return
		}
	}
}

func TestIntegerLiteralExpressions(t *testing.T) {
	input := "5;"

	lex := lexer.New(input)
	par := parser.New(lex)
	program := par.ParseProgram()
	checkParserErrors(t, par)

	if len(program.Statements) != 1 {
		t.Fatalf("Expected 1 statement, got %d", len(program.Statements))
	}

	stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
	if !ok {
		t.Fatalf("Statement received not an expression statement. Got %T", program.Statements[0])
	}

	ident, ok := stmt.Expr.(*ast.IntegerLiteral)
	if !ok {
		t.Fatalf("Expr received not integer literal. Got %T", stmt.Expr)
	}
	if ident.Value != 5 {
		t.Errorf("ident.Value not %d, got %d", 5, ident.Value)
	}
	if ident.TokenLiteral() != "5" {
		t.Errorf("ident.TokenLiteral not %s, got %s", "5", ident.TokenLiteral())
	}
}

func TestPrefixExpressions(t *testing.T) {
	prefixTests := []struct {
		input    string
		operator string
		value    interface{}
	}{
		{"!5;", "!", 5},
		{"-15;", "-", 15},
		{"!foobar;", "!", "foobar"},
		{"-foobar;", "-", "foobar"},
		{"!true;", "!", true},
		{"!false;", "!", false},
	}
	for _, tt := range prefixTests {
		lex := lexer.New(tt.input)
		par := parser.New(lex)
		program := par.ParseProgram()
		checkParserErrors(t, par)

		if len(program.Statements) != 1 {
			t.Fatalf("Expected 1 statement, got %d", len(program.Statements))
		}

		stmt, ok := program.Statements[0].(*ast.ExpressionStatement)
		if !ok {
			t.Fatalf("Statement received not an expression statement. Got %T", program.Statements[0])
		}

		expr, ok := stmt.Expr.(*ast.PrefixExpression)
		if !ok {
			t.Fatalf("Statement was not a prefix expression, got %T", stmt.Expr)
		}
		if expr.Operator != tt.operator {
			t.Fatalf("Operator was not a %s, got %s", tt.operator, expr.Operator)
		}
		if !testLiteralExpression(t, expr.Right, tt.value) {
			return
		}
	}
}
