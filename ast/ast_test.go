package ast_test

import (
	"testing"

	"gitlab.com/darkwyrm/mifflin/ast"
	"gitlab.com/darkwyrm/mifflin/tokens"
)

func TestString(t *testing.T) {
	program := &ast.Program{
		Statements: []ast.Statement{
			&ast.VarStatement{
				Token: tokens.Token{Type: tokens.VAR, Literal: "var"},
				Name: &ast.Identifier{
					Token: tokens.Token{Type: tokens.IDENTIFIER, Literal: "myVar"},
					Value: "myVar",
				},
				Value: &ast.Identifier{
					Token: tokens.Token{Type: tokens.IDENTIFIER, Literal: "anotherVar"},
					Value: "anotherVar",
				},
			},
		},
	}

	if program.String() != "var myVar = anotherVar; " {
		t.Errorf("program.String() wrong, got %q", program.String())
	}
}
