package repl

import (
	"bufio"
	"fmt"
	"gitlab.com/darkwyrm/mifflin/eval"
	"gitlab.com/darkwyrm/mifflin/obj"
	"gitlab.com/darkwyrm/mifflin/parser"
	"io"
	"strings"

	"gitlab.com/darkwyrm/mifflin/lexer"
)

const PROMPT = "» "

func Start(in io.Reader, out io.Writer) {
	fmt.Print("Welcome to Mifflin's interactive environment.\n")
	fmt.Print("Use the '\\q' command or press Ctrl+D on a blank line to exit.\n\n")

	scanner := bufio.NewScanner(in)
	env := obj.NewEnvironment()

	for {
		fmt.Print(PROMPT)
		scanned := scanner.Scan()
		if !scanned {
			return
		}

		line := scanner.Text()
		if line == "" {
			continue
		}
		if strings.ToLower(line) == `\q` {
			break
		}
		lex := lexer.New(line)
		par := parser.New(lex)

		program := par.ParseProgram()
		if len(par.Errors()) != 0 {
			PrintParserErrors(out, par.Errors())
			continue
		}

		evaluated := eval.Eval(program, env)
		if evaluated != nil {
			outStr := evaluated.Inspect()
			if outStr != "none" {
				_, _ = io.WriteString(out, outStr+"\n")
			}
		}
	}
}

func PrintParserErrors(out io.Writer, errors []string) {
	for _, msg := range errors {
		_, _ = fmt.Fprintln(out, "\t"+msg)
	}
}
