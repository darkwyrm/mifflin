package main

import (
	"fmt"
	"gitlab.com/darkwyrm/mifflin/eval"
	"gitlab.com/darkwyrm/mifflin/lexer"
	"gitlab.com/darkwyrm/mifflin/obj"
	"gitlab.com/darkwyrm/mifflin/parser"
	"gitlab.com/darkwyrm/mifflin/repl"
	"io"
	"log"
	"os"
	"strings"
)

const PROGRAMVERSION = "0.1"
const PROGRAMCOPYRIGHT = "2024 Jon Yoder"

func main() {
	if len(os.Args) < 2 {
		repl.Start(os.Stdin, os.Stderr)
		os.Exit(0)
	}

	switch strings.ToLower(os.Args[1]) {
	case "--version", "-v", "-h", "--help":
		fmt.Printf("mifflin %s\n©%s\n\n", PROGRAMVERSION, PROGRAMCOPYRIGHT)
		os.Exit(0)
	default:
		runFile(os.Args[1])
	}
}

func runFile(name string) bool {
	data, err := os.ReadFile(name)
	if err != nil {
		log.Fatal(err)
		return false
	}

	lex := lexer.New(string(data))
	par := parser.New(lex)

	program := par.ParseProgram()
	if len(par.Errors()) != 0 {
		repl.PrintParserErrors(os.Stderr, par.Errors())
		return false
	}

	evaluated := eval.Eval(program, obj.NewEnvironment())
	if evaluated != nil {
		outStr := evaluated.Inspect()
		if outStr != "none" {
			_, _ = io.WriteString(os.Stderr, outStr+"\n")
		}
	}
	return true
}
