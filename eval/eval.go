package eval

import (
	"fmt"
	"gitlab.com/darkwyrm/mifflin/obj"
	"math"
	"strings"
)
import "gitlab.com/darkwyrm/mifflin/ast"

func Eval(node ast.Node, env *obj.Environment) obj.Object {
	switch node := node.(type) {
	case *ast.Program:
		return evalProgram(node, env)
	case *ast.BlockStatement:
		return evalBlockStatement(node, env)
	case *ast.ExpressionStatement:
		return Eval(node.Expr, env)
	case *ast.BreakStatement:
		return &obj.BreakValue{}
	case *ast.ContinueStatement:
		return &obj.ContinueValue{}
	case *ast.ReturnStatement:
		val := Eval(node.ReturnValue, env)
		if isError(val) {
			return val
		}
		return &obj.ReturnValue{Value: val}
	case *ast.ConstStatement:
		val := Eval(node.Value, env)
		if isError(val) {
			return val
		}
		env.Define(node.Name.Value, makeConstant(val))
	case *ast.VarStatement:
		val := Eval(node.Value, env)
		if isError(val) {
			return val
		}
		return env.Define(node.Name.Value, val)
	case *ast.WhileStatement:
		val := evalWhileStatement(node, env)
		if isError(val) {
			return val
		}
	case *ast.Identifier:
		return evalIdentifier(node, env)

	case *ast.CallExpression:
		function := Eval(node.Function, env)
		if isError(function) {
			return function
		}

		args := evalExpressions(node.Arguments, env)
		if len(args) == 1 && isError(args[0]) {
			return args[0]
		}
		return applyFunction(function, args)
	case *ast.IfExpression:
		return evalIfExpression(node, env)
	case *ast.IndexExpression:
		left := Eval(node.Left, env)
		if isError(left) {
			return left
		}
		index := Eval(node.Index, env)
		if isError(index) {
			return index
		}
		return evalIndexExpression(left, index)

	case *ast.InfixExpression:
		switch node.Operator {
		case "=", "+=", "-=", "*=", "/=", "%=":
			return evalAssignmentExpression(node.Operator, node.Left, node.Right, env)
		}

		left := Eval(node.Left, env)
		if isError(left) {
			return left
		}

		right := Eval(node.Right, env)
		if isError(right) {
			return right
		}

		return evalInfixExpression(node.Operator, left, right)
	case *ast.PrefixExpression:
		right := Eval(node.Right, env)
		if isError(right) {
			return right
		}
		return evalPrefixExpression(node.Operator, right)
	case *ast.BooleanLiteral:
		if node.Value {
			return obj.TRUE
		} else {
			return obj.FALSE
		}
	case *ast.FloatLiteral:
		return &obj.Float{Value: node.Value}
	case *ast.FunctionLiteral:
		params := node.Parameters
		body := node.Body
		return &obj.Function{Parameters: params, Env: env, Body: body}
	case *ast.IntegerLiteral:
		return &obj.Integer{Value: node.Value}
	case *ast.ListLiteral:
		elements := evalExpressions(node.Elements, env)
		if len(elements) == 1 && isError(elements[0]) {
			return elements[0]
		}
		return &obj.List{Elements: elements}
	case *ast.NoneLiteral:
		return obj.NONE
	case *ast.StringLiteral:
		return &obj.String{Value: node.Value}
	case *ast.UIntegerLiteral:
		return &obj.UInteger{Value: node.Value}
	case *ast.UndefinedLiteral:
		return obj.UNDEFINED
	}
	return nil
}

func makeConstant(val obj.Object) obj.Object {
	switch val.(type) {
	case *obj.Boolean:
		(val.(*obj.Boolean)).Constant = true
	case *obj.Float:
		(val.(*obj.Float)).Constant = true
	case *obj.Integer:
		(val.(*obj.Integer)).Constant = true
	case *obj.String:
		(val.(*obj.String)).Constant = true
	case *obj.UInteger:
		(val.(*obj.UInteger)).Constant = true
	}
	return val
}

func evalProgram(program *ast.Program, env *obj.Environment) obj.Object {
	var result obj.Object

	for _, statement := range program.Statements {
		result = Eval(statement, env)

		switch result := result.(type) {
		case *obj.ReturnValue:
			return result.Value
		case *obj.Error:
			return result
		}
	}

	return result
}

func evalBlockStatement(block *ast.BlockStatement, env *obj.Environment) obj.Object {
	var result obj.Object

	for _, statement := range block.Statements {
		result = Eval(statement, env)

		if result != nil {
			rt := result.Type()
			switch rt {
			case obj.RETURNOBJ, obj.BREAKOBJ, obj.CONTINUEOBJ, obj.ERROROBJ:
				return result
			}

		}
	}

	return result
}

func evalWhileStatement(node *ast.WhileStatement, env *obj.Environment) obj.Object {
	cond := Eval(node.Condition, env)
	if isError(cond) {
		return cond
	}
	if cond.Type() != obj.BOOLEANOBJ {
		return newError("While condition does not evaluate to a Boolean value")
	}
	for cond.(*obj.Boolean).Value {
		childEnv := obj.NewChildEnvironment(env)
		result := Eval(node.Body, childEnv)
		if isError(result) {
			return result
		}
		if result.Type() == obj.BREAKOBJ {
			return obj.NONE
		}
		cond = Eval(node.Condition, env)
		if isError(cond) {
			return cond
		}
		if cond.Type() != obj.BOOLEANOBJ {
			return newError("While condition does not evaluate to a Boolean value")
		}
	}
	return obj.NONE
}

func evalAssignmentExpression(Operator string, left ast.Node, right ast.Node,
	env *obj.Environment) obj.Object {

	id, ok := left.(*ast.Identifier)
	if !ok {
		return newError("%s is not something you can assign a value to", left.String())
	}

	rightValue := Eval(right, env)
	if isError(rightValue) {
		return rightValue
	}

	if Operator == "=" {
		return env.Assign(id.Value, rightValue)
	}

	idValue := env.Get(id.Value)
	if isError(idValue) {
		return idValue
	}
	modOp, _ := strings.CutSuffix(Operator, "=")
	idValue = evalInfixExpression(modOp, idValue, rightValue)
	if isError(idValue) {
		return idValue
	}

	return env.Assign(id.Value, idValue)
}

func evalInfixExpression(operator string, left obj.Object, right obj.Object) obj.Object {
	switch {
	case left.Type() == obj.INTOBJ:
		if left.Type() != right.Type() {
			return newError("type mismatch: %s %s %s", left.Type(), operator, right.Type())
		}
		return evalIntegerInfixExpression(operator, left, right)
	case left.Type() == obj.UINTOBJ:
		if left.Type() != right.Type() {
			return newError("type mismatch: %s %s %s", left.Type(), operator, right.Type())
		}
		return evalUIntegerInfixExpression(operator, left, right)
	case left.Type() == obj.FLOATOBJ:
		if left.Type() != right.Type() {
			return newError("type mismatch: %s %s %s", left.Type(), operator, right.Type())
		}
		return evalFloatInfixExpression(operator, left, right)
	case left.Type() == obj.STRINGOBJ:
		return evalStringInfixExpression(operator, left, right)
	case operator == "==":
		return nativeBoolToBooleanObject(left == right)
	case operator == "!=":
		return nativeBoolToBooleanObject(left != right)
	case left.Type() == obj.BOOLEANOBJ:
		if left.Type() != right.Type() {
			return newError("type mismatch: %s %s %s", left.Type(), operator, right.Type())
		}
		return evalBooleanInfixExpression(operator, left, right)
	default:
		return newError("unknown operation: %s %s %s",
			left.Type(), operator, right.Type())
	}
}

func evalPrefixExpression(operator string, right obj.Object) obj.Object {
	switch operator {
	case "!":
		return evalBangOperatorExpression(right)
	case "-":
		return evalNegationOperatorExpression(right)
	default:
		return newError("unknown operation: %s%s", operator, right.Type())
	}
}

func evalIfExpression(ie *ast.IfExpression, env *obj.Environment) obj.Object {
	condition := Eval(ie.Condition, env)

	if condition == obj.TRUE {
		return Eval(ie.Consequence, env)
	} else if ie.Alternative != nil {
		return Eval(ie.Alternative, env)
	} else {
		return obj.NONE
	}
}

func evalBangOperatorExpression(right obj.Object) obj.Object {
	switch right {
	case obj.TRUE:
		return obj.FALSE
	case obj.FALSE:
		return obj.TRUE
	default:
		return newError("unknown operation: !%s", right.Type())
	}
}

func evalNegationOperatorExpression(right obj.Object) obj.Object {
	switch right.Type() {
	case obj.INTOBJ:
		return &obj.Integer{Value: -right.(*obj.Integer).Value}
	case obj.FLOATOBJ:
		return &obj.Float{Value: -right.(*obj.Float).Value}
	default:
		return newError("unknown operation: -%s", right.Type())
	}
}

func evalBooleanInfixExpression(operator string, left, right obj.Object) obj.Object {
	leftVal := left.(*obj.Boolean).Value
	rightVal := right.(*obj.Boolean).Value

	switch operator {
	case "and":
		return nativeBoolToBooleanObject(leftVal && rightVal)
	case "or":
		return nativeBoolToBooleanObject(leftVal || rightVal)
	default:
		return newError("unrecognized operation: %s(%s) %s %s(%s)", left.Type(),
			left.Inspect(), operator, right.Type(), right.Inspect())
	}
}

func evalFloatInfixExpression(operator string, left, right obj.Object) obj.Object {
	leftVal := left.(*obj.Float).Value
	rightVal := right.(*obj.Float).Value

	switch operator {
	case "+":
		return &obj.Float{Value: leftVal + rightVal}
	case "-":
		return &obj.Float{Value: leftVal - rightVal}
	case "*":
		return &obj.Float{Value: leftVal * rightVal}
	case "/":
		return &obj.Float{Value: leftVal / rightVal}
	case "**":
		return &obj.Float{Value: math.Pow(leftVal, rightVal)}
	case "%":
		return &obj.Float{Value: math.Mod(leftVal, rightVal)}
	case "<":
		return nativeBoolToBooleanObject(leftVal < rightVal)
	case ">":
		return nativeBoolToBooleanObject(leftVal > rightVal)
	case "==":
		return nativeBoolToBooleanObject(leftVal == rightVal)
	case "!=":
		return nativeBoolToBooleanObject(leftVal != rightVal)
	default:
		return newError("unrecognized operation: %s(%s) %s %s(%s)", left.Type(),
			left.Inspect(), operator, right.Type(), right.Inspect())
	}
}

func evalIntegerInfixExpression(operator string, left, right obj.Object) obj.Object {
	leftVal := left.(*obj.Integer).Value
	rightVal := right.(*obj.Integer).Value

	switch operator {
	case "+":
		return &obj.Integer{Value: leftVal + rightVal}
	case "-":
		return &obj.Integer{Value: leftVal - rightVal}
	case "*":
		return &obj.Integer{Value: leftVal * rightVal}
	case "/":
		return &obj.Integer{Value: leftVal / rightVal}
	case "**":
		return &obj.Integer{Value: intPow(leftVal, rightVal)}
	case "%":
		return &obj.Integer{Value: leftVal % rightVal}
	case "<":
		return nativeBoolToBooleanObject(leftVal < rightVal)
	case ">":
		return nativeBoolToBooleanObject(leftVal > rightVal)
	case "==":
		return nativeBoolToBooleanObject(leftVal == rightVal)
	case "!=":
		return nativeBoolToBooleanObject(leftVal != rightVal)
	default:
		return newError("unrecognized operation: %s(%s) %s %s(%s)", left.Type(),
			left.Inspect(), operator, right.Type(), right.Inspect())
	}
}

func evalStringInfixExpression(operator string, left, right obj.Object) obj.Object {
	switch operator {
	case "+":
		if right.Type() != obj.STRINGOBJ {
			return newError("String contatenation only works with strings")
		}
		return &obj.String{Value: left.(*obj.String).Value + right.(*obj.String).Value}
	case "-":
		if right.Type() != obj.STRINGOBJ {
			return newError("String removal only works with strings")
		}
		return &obj.String{Value: strings.Replace(
			left.(*obj.String).Value, right.(*obj.String).Value, "", -1,
		)}
	case "*":
		switch right.Type() {
		case obj.INTOBJ:
			return &obj.String{Value: strings.Repeat(left.Inspect(), int(right.(*obj.Integer).Value))}
		case obj.UINTOBJ:
			return &obj.String{Value: strings.Repeat(left.Inspect(), int(right.(*obj.UInteger).Value))}
		default:
			return newError("String repetition only works with integers")
		}
	default:
		return newError("unrecognized operation: %s(%s) %s %s(%s)", left.Type(),
			left.Inspect(), operator, right.Type(), right.Inspect())
	}
}

func evalUIntegerInfixExpression(operator string, left, right obj.Object) obj.Object {
	leftVal := left.(*obj.UInteger).Value
	rightVal := right.(*obj.UInteger).Value

	switch operator {
	case "+":
		return &obj.UInteger{Value: leftVal + rightVal}
	case "-":
		return &obj.UInteger{Value: leftVal - rightVal}
	case "*":
		return &obj.UInteger{Value: leftVal * rightVal}
	case "/":
		return &obj.UInteger{Value: leftVal / rightVal}
	case "**":
		return &obj.UInteger{Value: uintPow(leftVal, rightVal)}
	case "%":
		return &obj.UInteger{Value: leftVal % rightVal}
	case "<":
		return nativeBoolToBooleanObject(leftVal < rightVal)
	case ">":
		return nativeBoolToBooleanObject(leftVal > rightVal)
	case "==":
		return nativeBoolToBooleanObject(leftVal == rightVal)
	case "!=":
		return nativeBoolToBooleanObject(leftVal != rightVal)
	default:
		return newError("unrecognized operation: %s(%s) %s %s(%s)", left.Type(),
			left.Inspect(), operator, right.Type(), right.Inspect())
	}
}

func evalIdentifier(node *ast.Identifier, env *obj.Environment) obj.Object {
	val := env.Get(node.Value)

	if val != nil {
		return val
	}

	if builtin, ok := builtins[node.Value]; ok {
		return builtin
	}

	return newError("identifier not found: " + node.Value)
}

func evalExpressions(exps []ast.Expression, env *obj.Environment) []obj.Object {
	var result []obj.Object

	for _, e := range exps {
		evaluated := Eval(e, env)
		if isError(evaluated) {
			return []obj.Object{evaluated}
		}
		result = append(result, evaluated)
	}

	return result
}

func evalIndexExpression(left, index obj.Object) obj.Object {
	switch {
	case left.Type() == obj.LISTOBJ && index.Type() == obj.INTOBJ:
		return evalListIndexExpression(left, index)
	default:
		return newError("index operator uses list[int], not  %s[%s]", left.Type(), index.Type())
	}
}

func evalListIndexExpression(list, index obj.Object) obj.Object {
	listObject := list.(*obj.List)
	idx := index.(*obj.Integer).Value
	listMax := int64(len(listObject.Elements) - 1)

	if idx < 0 || idx > listMax {
		return obj.NONE
	}

	return listObject.Elements[idx]
}

func nativeBoolToBooleanObject(input bool) *obj.Boolean {
	if input {
		return obj.TRUE
	}
	return obj.FALSE
}

func newError(format string, a ...interface{}) *obj.Error {
	return &obj.Error{Message: fmt.Sprintf(format, a...)}
}

func isError(ob obj.Object) bool {
	if ob != nil {
		return ob.Type() == obj.ERROROBJ
	}
	return false
}

func applyFunction(fn obj.Object, args []obj.Object) obj.Object {
	switch fn := fn.(type) {

	case *obj.Function:
		extendedEnv := extendFunctionEnv(fn, args)
		evaluated := Eval(fn.Body, extendedEnv)
		return unwrapReturnValue(evaluated)

	case *obj.Builtin:
		return fn.Fn(args...)

	default:
		return newError("not a function: %s", fn.Type())
	}
}

func extendFunctionEnv(fn *obj.Function, args []obj.Object) *obj.Environment {
	env := obj.NewChildEnvironment(fn.Env)
	for paramIndex, param := range fn.Parameters {
		env.Define(param.Value, args[paramIndex])
	}
	return env
}

func unwrapReturnValue(ob obj.Object) obj.Object {
	if returnValue, ok := ob.(*obj.ReturnValue); ok {
		return returnValue.Value
	}
	return ob
}

func intPow(n, m int64) int64 {
	if m == 0 {
		return 1
	}
	if m == 1 {
		return n
	}

	result := n
	for i := int64(2); i <= m; i++ {
		result *= n
	}
	return result
}

func uintPow(n, m uint64) uint64 {
	if m == 0 {
		return 1
	}
	if m == 1 {
		return n
	}

	result := n
	for i := uint64(2); i <= m; i++ {
		result *= n
	}
	return result
}
