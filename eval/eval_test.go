package eval

import (
	"fmt"
	"gitlab.com/darkwyrm/mifflin/lexer"
	"gitlab.com/darkwyrm/mifflin/obj"
	"gitlab.com/darkwyrm/mifflin/parser"
	"testing"
)

func TestAssignExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"var a; a = 5; a;", 5},
		{"var a = 0; a += 5; a;", 5},
		{"var a = 10; a -= 5; a;", 5},
		{"var a = 1; a *= 5; a;", 5},
		{"var a = 25; a /= 5; a;", 5},
		{"var a = 15; a %= 10; a;", 5},
	}

	for _, tt := range tests {
		testIntegerObject(t, testEval(tt.input), tt.expected)
	}
}

func TestBreakStatements(t *testing.T) {
	input := `var x = 1; while x < 10 { if x == 5 { break; }; x += 1; }; x;`
	evaluated := testEval(input)
	testIntegerObject(t, evaluated, 5)
}

func TestContinueStatements(t *testing.T) {
	input := `var x = 1; var y = 1; 
while x < 10 { 
if x == 5 { x += 1; continue; }; 
x += 1;
y += 1; }; 
y;`
	evaluated := testEval(input)
	testIntegerObject(t, evaluated, 9)
}

func TestReturnStatements(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"return 10;", 10},
		{"return 10; 9;", 10},
		{"return 2 * 5; 9;", 10},
		{"9; return 2 * 5; 9;", 10},
		{"if 10 > 1 { return 10; };", 10},
		{`if 10 > 1 { if 10 > 1 { return 10; }; return 1; };`, 10},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testIntegerObject(t, evaluated, tt.expected)
	}
}

func TestVarStatements(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"var a = 5; a;", 5},
		{"var a = 5 * 5; a;", 25},
		{"var a = 5; var b = a; b;", 5},
		{"var a = 5; var b = a; var c = a + b + 5; c;", 15},
		{"const a = 5; a;", 5},
		{"const a = 5 * 5; a;", 25},
		{"const a = 5; const b = a; b;", 5},
		{"const a = 5; const b = a; const c = a + b + 5; c;", 15},
	}

	for _, tt := range tests {
		testIntegerObject(t, testEval(tt.input), tt.expected)
	}
}

func TestWhileStatements(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"var a = 1; while a < 5 { a = a + 1; } a;", 5},
		{"var a = 1; while a < 5 { a += 1; } a;", 5},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testIntegerObject(t, evaluated, tt.expected)
	}
}

func TestBuiltinFunctions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{`print("Hola ", "mundo! ");`, nil},
		{`println("Hello", " ", "world!");`, nil},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)

		switch expected := tt.expected.(type) {
		case int:
			testIntegerObject(t, evaluated, int64(expected))
		case nil:
			testNoneObject(t, evaluated)
		case string:
			errObj, ok := evaluated.(*obj.Error)
			if !ok {
				t.Errorf("object is not Error, got %T (%+v)",
					evaluated, evaluated)
				continue
			}
			if errObj.Message != expected {
				t.Errorf("wrong error message. Expected %q, got %q",
					expected, errObj.Message)
			}
		}
	}
}

func TestClosures(t *testing.T) {
	input := `
var first = 10;
var second = 10;
var third = 10;

const ourFunction = fn(first) {
  var second = 20;

  first + second + third;
};

ourFunction(20) + first + second;`

	testIntegerObject(t, testEval(input), 70)
}

func TestFunctionApplication(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"const identity = fn(x) { x; }; identity(5);", 5},
		{"const identity = fn(x) { return x; }; identity(5);", 5},
		{"const double = fn(x) { x * 2; }; double(5);", 10},
		{"const add = fn(x, y) { x + y; }; add(5, 5);", 10},
		{"const add = fn(x, y) { x + y; }; add(5 + 5, add(5, 5));", 20},
	}

	for _, tt := range tests {
		testIntegerObject(t, testEval(tt.input), tt.expected)
	}
}

func TestIfElseExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{"if true { 10; };", 10},
		{"if false { 10; };", nil},
		{"if 1 < 2 { 10; };", 10},
		{"if 1 > 2 { 10; };", nil},
		{"if 1 > 2 { 10; } else { 20; };", 20},
		{"if 1 < 2 { 10; } else { 20; };", 10},
		{"if true and true { 10; } else { 20; };", 10},
		{"if true and false { 10; } else { 20; };", 20},
		{"if false and true { 10; } else { 20; };", 20},
		{"if false and false { 10; } else { 20; };", 20},
		{"if true or true { 10; } else { 20; };", 10},
		{"if true or false { 10; } else { 20; };", 10},
		{"if false or true { 10; } else { 20; };", 10},
		{"if false or false { 10; } else { 20; };", 20},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		integer, ok := tt.expected.(int)
		if ok {
			testIntegerObject(t, evaluated, int64(integer))
		} else {
			testNoneObject(t, evaluated)
		}
	}
}

func TestBangOperator(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"!true;", false},
		{"!false;", true},
		{"!!true;", true},
		{"!!false;", false},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testBooleanObject(t, evaluated, tt.expected)
	}
}

func TestStringOperations(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{`"Hello" + " " + "World!";`, "Hello World!"},
		{`"abcdefabcdef" - "def";`, "abcabc"},
		{`"abc" * 3;`, "abcabcabc"},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		str, ok := evaluated.(*obj.String)
		if !ok {
			t.Fatalf("object is not String, got %T (%+v)", evaluated, evaluated)
		}

		if str.Value != tt.expected {
			t.Errorf("String has wrong value, expected %s got %s", tt.expected, str.Value)
		}
	}
}

func TestErrorHandling(t *testing.T) {
	tests := []struct {
		input           string
		expectedMessage string
	}{
		{
			"5 + true;",
			"type mismatch: int + bool",
		},
		{
			"5 + true; 5;",
			"type mismatch: int + bool",
		},
		{
			"-true;",
			"unknown operation: -bool",
		},
		{
			"true + false;",
			"unrecognized operation: bool(true) + bool(false)",
		},
		{
			"true + false + true + false;",
			"unrecognized operation: bool(true) + bool(false)",
		},
		{
			"5; true + false; 5;",
			"unrecognized operation: bool(true) + bool(false)",
		},
		{
			"if (10 > 1) { true + false; };",
			"unrecognized operation: bool(true) + bool(false)",
		},
		{`
if (10 > 1) {
  if (10 > 1) {
    return true + false;
  };

  return 1;
};
`, "unrecognized operation: bool(true) + bool(false)"},
		{
			"foobar;",
			"identifier not found: foobar",
		},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)

		errObj, ok := evaluated.(*obj.Error)
		if !ok {
			t.Errorf("no error object returned, got %T(%+v)",
				evaluated, evaluated)
			continue
		}

		if errObj.Message != tt.expectedMessage {
			t.Errorf("wrong error message. Expected %q, got %q",
				tt.expectedMessage, errObj.Message)
		}
	}
}

func TestEvalBooleanExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"true;", true},
		{"false;", false},
		{"1 < 2;", true},
		{"1 > 2;", false},
		{"1 < 1;", false},
		{"1 > 1;", false},
		{"1 == 1;", true},
		{"1 != 1;", false},
		{"1 == 2;", false},
		{"1 != 2;", true},
		{"true == true;", true},
		{"false == false;", true},
		{"true == false;", false},
		{"true != false;", true},
		{"false != true;", true},
		{"true and true;", true},
		{"true and false;", false},
		{"false and true;", false},
		{"false and false;", false},
		{"true or true;", true},
		{"false or true;", true},
		{"true or false;", true},
		{"false or false;", false},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testBooleanObject(t, evaluated, tt.expected)
	}
}

func TestEvalFloatExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected float64
	}{
		{"3.1;", 3.1},
		{"0.0;", 0.0},
		{"-3.1;", -3.1},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testFloatObject(t, evaluated, tt.expected)
	}
}

func TestEvalIntegerExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"5;", 5},
		{"10;", 10},
		{"-5;", -5},
		{"-10;", -10},
		{"5 + 5 + 5 + 5 - 10;", 10},
		{"2 * 2 * 2 * 2 * 2;", 32},
		{"-50 + 100 + -50;", 0},
		{"5 * 2 + 10;", 20},
		{"5 + 2 * 10;", 25},
		{"2 ** 3;", 8},
		{"6 % 5;", 1},
		{"20 + 2 * -10;", 0},
		{"50 / 2 * 2 + 10;", 60},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testIntegerObject(t, evaluated, tt.expected)
	}
}

func TestEvalStringExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{`"foobar";`, "foobar"},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testStringObject(t, evaluated, tt.expected)
	}
}

func TestEvalUIntegerExpression(t *testing.T) {
	tests := []struct {
		input    string
		expected uint64
	}{
		{"5u;", uint64(5)},
		{"10u;", uint64(10)},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		testUIntegerObject(t, evaluated, tt.expected)
	}
}

func TestListObject(t *testing.T) {
	input := "[1, 2 * 2, 3 + 3];"

	evaluated := testEval(input)
	result, ok := evaluated.(*obj.List)
	if !ok {
		t.Fatalf("object is not List, got %T (%+v)", evaluated, evaluated)
	}

	if len(result.Elements) != 3 {
		t.Fatalf("list has wrong num of elements, expected 3, got %d", len(result.Elements))
	}

	testIntegerObject(t, result.Elements[0], 1)
	testIntegerObject(t, result.Elements[1], 4)
	testIntegerObject(t, result.Elements[2], 6)
}

func TestListIndexExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected interface{}
	}{
		{
			"[1, 2, 3][0];",
			1,
		},
		{
			"[1, 2, 3][1];",
			2,
		},
		{
			"[1, 2, 3][2];",
			3,
		},
		{
			"var i = 0; [1][i];",
			1,
		},
		{
			"[1, 2, 3][1 + 1];",
			3,
		},
		{
			"var myList = [1, 2, 3]; myList[2];",
			3,
		},
		{
			"var myList = [1, 2, 3]; myList[0] + myList[1] + myList[2];",
			6,
		},
		{
			"var myList = [1, 2, 3]; var i = myList[0]; myList[i];",
			2,
		},
		{
			"[1, 2, 3][3];",
			nil,
		},
		{
			"[1, 2, 3][-1];",
			nil,
		},
	}

	for _, tt := range tests {
		evaluated := testEval(tt.input)
		integer, ok := tt.expected.(int)
		if ok {
			testIntegerObject(t, evaluated, int64(integer))
		} else {
			testNoneObject(t, evaluated)
		}
	}
}

func testBooleanObject(t *testing.T, ob obj.Object, expected bool) bool {
	result, ok := ob.(*obj.Boolean)
	if !ok {
		t.Errorf("object is not Boolean, got %T (%+v)", ob, ob)
		return false
	}
	if result.Value != expected {
		t.Errorf("Incorrect object value. Expected %t, got %t", result.Value, expected)
		return false
	}
	return true
}

func testEval(input string) obj.Object {
	l := lexer.New(input)
	p := parser.New(l)
	program := p.ParseProgram()
	env := obj.NewEnvironment()

	if len(p.Errors()) > 0 {
		fmt.Printf("Parser had %d errors:\n", len(p.Errors()))
		for _, msg := range p.Errors() {
			fmt.Printf("Parser error: %q\n", msg)
		}
		fmt.Printf("\nProgram listing:\n\n")
		fmt.Println(program.String())
		panic("")
	}

	return Eval(program, env)
}

func testFloatObject(t *testing.T, ob obj.Object, expected float64) bool {
	result, ok := ob.(*obj.Float)
	if !ok {
		t.Errorf("object is not Float, got %T (%+v)", ob, ob)
		return false
	}
	if result.Value != expected {
		t.Errorf("Incorrect object value. Expected %f, got %f", expected, result.Value)
		return false
	}

	return true
}

func testIntegerObject(t *testing.T, ob obj.Object, expected int64) bool {
	result, ok := ob.(*obj.Integer)
	if !ok {
		t.Errorf("object is not Integer, got %T (%+v)", ob, ob)
		return false
	}
	if result.Value != expected {
		t.Errorf("Incorrect object value. Expected %d, got %d", expected, result.Value)
		return false
	}

	return true
}

func testNoneObject(t *testing.T, ob obj.Object) bool {
	if ob != obj.NONE {
		t.Errorf("object is not NONE, got %T (%+v)", ob, ob)
		return false
	}
	return true
}

func testStringObject(t *testing.T, ob obj.Object, expected string) bool {
	result, ok := ob.(*obj.String)
	if !ok {
		t.Errorf("object is not String, got %T (%+v)", ob, ob)
		return false
	}
	if result.Value != expected {
		t.Errorf("Incorrect object value. Expected %s, got %s", expected, result.Value)
		return false
	}

	return true
}

func testUIntegerObject(t *testing.T, ob obj.Object, expected uint64) bool {
	result, ok := ob.(*obj.UInteger)
	if !ok {
		t.Errorf("object is not UInteger, got %T (%+v)", ob, ob)
		return false
	}
	if result.Value != expected {
		t.Errorf("Incorrect object value. Expected %d, got %d", result.Value, expected)
		return false
	}

	return true
}
