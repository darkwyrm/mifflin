package eval

import (
	"testing"
)

func TestAssignAndScopeExpressions(t *testing.T) {
	tests := []struct {
		input    string
		expected int64
	}{
		{"var a = 10; if a > 1 { a + 10; };", 20},
		{"var a = 10; if a > 1 { a += 10; a; };", 20},
		{"var a = 10; a = a + 10; a;", 20},
		{"var a = 10; if a > 1 { a = a + 10; a; };", 20},
	}

	for _, tt := range tests {
		testIntegerObject(t, testEval(tt.input), tt.expected)
	}
}
