package eval

import (
	"bufio"
	"fmt"
	"gitlab.com/darkwyrm/mifflin/obj"
	"os"
)

var builtins = map[string]*obj.Builtin{
	"exit": {
		Fn: func(args ...obj.Object) obj.Object {
			if err := expectArgCount("exit", 1, len(args)); err != nil {
				return err
			}
			if args[0].Type() != obj.INTOBJ {
				return newError("exit() requires an integer argument, not %s", args[0].Type())
			}
			os.Exit(int(args[0].(*obj.Integer).Value))

			//  unreachable
			return obj.NONE
		},
	},
	"panic": {
		Fn: func(args ...obj.Object) obj.Object {
			if err := expectArgCount("panic", 1, len(args)); err != nil {
				return err
			}
			if args[0].Type() != obj.STRINGOBJ {
				return newError("panic() requires a string argument, not %s", args[0].Type())
			}
			println(args[0].(*obj.String).Value)
			os.Exit(-1)
			return obj.NONE
		},
	},
	"print": {
		Fn: func(args ...obj.Object) obj.Object {
			for _, arg := range args {
				fmt.Print(arg.Inspect())
			}
			return obj.NONE
		},
	},
	"println": {
		Fn: func(args ...obj.Object) obj.Object {
			for _, arg := range args {
				fmt.Print(arg.Inspect())
			}
			fmt.Print("\n")
			return obj.NONE
		},
	},
	"prompt": {
		Fn: func(args ...obj.Object) obj.Object {
			if err := expectArgCount("prompt", 1, len(args)); err != nil {
				return err
			}
			if args[0].Type() != obj.STRINGOBJ {
				return newError("prompt() requires a string argument, not %s", args[0].Type())
			}
			scanner := bufio.NewScanner(os.Stdin)

			prompt := args[0].(*obj.String).Value
			if len(prompt) > 0 {
				fmt.Print(prompt)
			}
			ok := scanner.Scan()
			if !ok {
				return &obj.String{Value: ""}
			}
			return &obj.String{Value: scanner.Text()}
		},
	},
	"type": {
		Fn: func(args ...obj.Object) obj.Object {
			if err := expectArgCount("type", 1, len(args)); err != nil {
				return err
			}
			return &obj.String{Value: string(args[0].Type())}
		},
	},
}

func expectArgCount(name string, count int, expected int) *obj.Error {
	if count == expected {
		return nil
	}
	if count > expected {
		return newError("Too many arguments for %s. %d needed and received %d", name, expected,
			count)
	}
	return newError("Arguments missing for %s. %d needed and received %d", name, expected, count)
}
