package tokens

import "fmt"

type TokenType uint

type Token struct {
	Type    TokenType
	Literal string
	Line    int
}

func (t Token) ToString() string {
	return fmt.Sprintf("T(%s %s %d)", TokenTypeToString(t.Type), t.Literal, t.Line)
}

const (
	// LPAREN and friends are 1-character tokens
	LPAREN = iota
	RPAREN
	LBRACKET
	RBRACKET
	LBRACE
	RBRACE
	COMMA
	DOT
	COLON
	MINUS
	MINUSEQ
	PLUS
	PLUSEQ
	SEMICOLON
	SLASH
	SLASHEQ
	STAR
	STAREQ
	DBLSTAR
	PERCENT
	PERCENTEQ
	HASH

	// BANG and company are 1-2 character tokens
	BANG
	BANGEQ
	EQ
	EQEQ
	LT
	LTEQ
	GT
	GTEQ

	// IDENTIFIER and the others are literals
	IDENTIFIER
	STRING
	UINT
	INT
	FLOAT

	// AND and other keywords
	AND
	BREAK
	CLASS
	CONST
	CONTINUE
	ELSE
	FALSE
	FN
	FOR
	IF
	NONE
	OR
	RETURN
	SELF
	SUPER
	TRUE
	VAR
	WHILE

	ILLEGAL
	EOF
)

func TokenTypeToString(t TokenType) string {
	out, ok := tokenTypeNames[t]
	if !ok {
		return fmt.Sprintf("UnknownTokenType(%d)", t)
	}
	return out
}

var tokenTypeNames = map[TokenType]string{
	LPAREN:    "LPAREN",
	RPAREN:    "RPAREN",
	LBRACKET:  "LBRACKET",
	RBRACKET:  "RBRACKET",
	LBRACE:    "LBRACE",
	RBRACE:    "RBRACE",
	COMMA:     "COMMA",
	DOT:       "DOT",
	COLON:     "COLON",
	MINUS:     "MINUS",
	PLUS:      "PLUS",
	SEMICOLON: "SEMICOLON",
	SLASH:     "SLASH",
	STAR:      "STAR",
	DBLSTAR:   "DBLSTAR",
	PERCENT:   "PERCENT",
	HASH:      "HASH",

	BANG:   "BANG",
	BANGEQ: "BANGEQ",
	EQ:     "EQ",
	EQEQ:   "EQEQ",
	LT:     "LT",
	LTEQ:   "LTEQ",
	GT:     "GT",
	GTEQ:   "GTEQ",

	IDENTIFIER: "IDENTIFIER",
	STRING:     "STRING",
	UINT:       "UINT",
	INT:        "INT",
	FLOAT:      "FLOAT",

	AND:      "AND",
	BREAK:    "BREAK",
	CLASS:    "CLASS",
	CONST:    "CONST",
	CONTINUE: "CONTINUE",
	ELSE:     "ELSE",
	FALSE:    "FALSE",
	FN:       "FN",
	FOR:      "FOR",
	IF:       "IF",
	NONE:     "NONE",
	OR:       "OR",
	RETURN:   "RETURN",
	SELF:     "SELF",
	SUPER:    "SUPER",
	TRUE:     "TRUE",
	VAR:      "VAR",
	WHILE:    "WHILE",

	ILLEGAL: "ILLEGAL",
	EOF:     "EOF",
}

var keywords = map[string]TokenType{
	"and":      AND,
	"break":    BREAK,
	"class":    CLASS,
	"const":    CONST,
	"continue": CONTINUE,
	"else":     ELSE,
	"false":    FALSE,
	"fn":       FN,
	"if":       IF,
	"none":     NONE,
	"or":       OR,
	"return":   RETURN,
	"super":    SUPER,
	"true":     TRUE,
	"var":      VAR,
	"while":    WHILE,
}

// StringToTokenType returns a token type given a specific string. Note that this only
// differentiates between keywords and identifiers
func StringToTokenType(ident string) TokenType {
	if tok, ok := keywords[ident]; ok {
		return tok
	}
	return IDENTIFIER
}
