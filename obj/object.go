package obj

import (
	"bytes"
	"fmt"
	"gitlab.com/darkwyrm/mifflin/ast"
	"strings"
)

var (
	TRUE      = &Boolean{Value: true}
	FALSE     = &Boolean{Value: false}
	NONE      = &None{}
	UNDEFINED = &Undefined{}
)

type BuiltinFunction func(args ...Object) Object

type ObjectType string

type Object interface {
	Type() ObjectType
	Inspect() string
	IsConstant() bool
}

const (
	INTOBJ      = "int"
	UINTOBJ     = "uint"
	FLOATOBJ    = "float"
	STRINGOBJ   = "string"
	BOOLEANOBJ  = "bool"
	NONEOBJ     = "none"
	BREAKOBJ    = "BREAK"
	CONTINUEOBJ = "CONTINUE"
	RETURNOBJ   = "RETURN"
	ERROROBJ    = "ERROR"
	FUNOBJ      = "fn"
	LISTOBJ     = "list"
	STDLIBOBJ   = "fn"
	UNDEFOBJ    = "undefined"
)

type Integer struct {
	Value    int64
	Constant bool
}

func (i *Integer) Inspect() string  { return fmt.Sprintf("%d", i.Value) }
func (i *Integer) Type() ObjectType { return INTOBJ }
func (i *Integer) IsConstant() bool { return i.Constant }

type Float struct {
	Value    float64
	Constant bool
}

func (f *Float) Inspect() string  { return fmt.Sprintf("%f", f.Value) }
func (f *Float) Type() ObjectType { return FLOATOBJ }
func (f *Float) IsConstant() bool { return f.Constant }

type String struct {
	Value    string
	Constant bool
}

func (s *String) Inspect() string  { return s.Value }
func (s *String) Type() ObjectType { return STRINGOBJ }
func (s *String) IsConstant() bool { return s.Constant }

type Boolean struct {
	Value    bool
	Constant bool
}

func (b *Boolean) Inspect() string {
	if b.Value {
		return "true"
	} else {
		return "false"
	}
}
func (b *Boolean) Type() ObjectType { return BOOLEANOBJ }
func (b *Boolean) IsConstant() bool { return b.Constant }

type UInteger struct {
	Value    uint64
	Constant bool
}

func (i *UInteger) Inspect() string  { return fmt.Sprintf("%d", i.Value) }
func (i *UInteger) Type() ObjectType { return UINTOBJ }
func (i *UInteger) IsConstant() bool { return i.Constant }

type None struct{}

func (n *None) Inspect() string  { return "none" }
func (n *None) Type() ObjectType { return NONEOBJ }
func (n *None) IsConstant() bool { return true }

type BreakValue struct {
	Value Object
}

func (bv *BreakValue) Type() ObjectType { return BREAKOBJ }
func (bv *BreakValue) Inspect() string  { return bv.Value.Inspect() }
func (bv *BreakValue) IsConstant() bool { return false }

type ContinueValue struct {
	Value Object
}

func (cv *ContinueValue) Type() ObjectType { return CONTINUEOBJ }
func (cv *ContinueValue) Inspect() string  { return cv.Value.Inspect() }
func (cv *ContinueValue) IsConstant() bool { return false }

type ReturnValue struct {
	Value Object
}

func (rv *ReturnValue) Type() ObjectType { return RETURNOBJ }
func (rv *ReturnValue) Inspect() string  { return rv.Value.Inspect() }
func (rv *ReturnValue) IsConstant() bool { return false }

type Error struct {
	Message string
}

func (e *Error) Type() ObjectType { return ERROROBJ }
func (e *Error) Inspect() string  { return "ERROR: " + e.Message }
func (e *Error) IsConstant() bool { return true }

type Function struct {
	Parameters []*ast.Identifier
	Body       *ast.BlockStatement
	Env        *Environment
}

func (f *Function) Type() ObjectType { return FUNOBJ }
func (f *Function) Inspect() string {
	var out bytes.Buffer

	params := make([]string, 0)
	for _, p := range f.Parameters {
		params = append(params, p.String())
	}

	out.WriteString("fn")
	out.WriteString("(")
	out.WriteString(strings.Join(params, ", "))
	out.WriteString(") {\n")
	out.WriteString(f.Body.String())
	out.WriteString("\n}")

	return out.String()
}
func (f *Function) IsConstant() bool { return false }

type Builtin struct {
	Fn BuiltinFunction
}

func (b *Builtin) Type() ObjectType { return STDLIBOBJ }
func (b *Builtin) Inspect() string  { return "stdlib function" }
func (b *Builtin) IsConstant() bool { return true }

type List struct {
	Elements []Object
	Constant bool
}

func (li *List) Type() ObjectType { return LISTOBJ }
func (li *List) Inspect() string {
	var out bytes.Buffer

	elements := make([]string, 0)
	for _, e := range li.Elements {
		elements = append(elements, e.Inspect())
	}

	out.WriteString("[")
	out.WriteString(strings.Join(elements, ", "))
	out.WriteString("]")

	return out.String()
}
func (li *List) IsConstant() bool { return li.Constant }

type Undefined struct{}

func (u *Undefined) Inspect() string  { return "undefined" }
func (u *Undefined) Type() ObjectType { return UNDEFOBJ }
func (u *Undefined) IsConstant() bool { return true }
