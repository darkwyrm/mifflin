package obj

import "testing"

func TestEnvironment(t *testing.T) {
	env := NewEnvironment()

	if env.Get("foo") != nil {
		t.Fatal("Get() returns OK on nonexistent key")
	}

	assert(t, env.Define("foo", TRUE) == NONE, "Error setting foo")
	assert(t, env.Define("foo", TRUE) != NONE, "No error redefining foo")

	result := env.Get("foo")
	if result.Type() == ERROROBJ {
		t.Fatal("Get() returns error on key 'foo'")
	}

	assert(t, env.Assign("bar", UNDEFINED) != NONE, "No error assigning to undefined bar")
	assert(t, env.Assign("foo", TRUE) == NONE, "Error assigning to foo")

	constTrue := &Boolean{Value: true, Constant: true}
	assert(t, env.Assign("foo", constTrue) == NONE, "Error making foo a constant")
	assert(t, env.Assign("foo", FALSE) != NONE, "No error reassigning constant")
}

func assert(t *testing.T, expr bool, msg string, args ...interface{}) {
	if !expr {
		t.Error(msg, args)
	}
}
