package obj

import "fmt"

func NewChildEnvironment(parent *Environment) *Environment {
	env := NewEnvironment()
	env.parent = parent
	return env
}

func NewEnvironment() *Environment {
	s := make(map[string]Object)
	return &Environment{store: s, parent: nil}
}

type Environment struct {
	store  map[string]Object
	parent *Environment
}

func (e *Environment) Assign(name string, val Object) Object {
	id, ok := e.store[name]
	if !ok {
		if e.parent != nil {
			return e.parent.Assign(name, val)
		}
		return &Error{Message: fmt.Sprintf("Assignment to undefined variable %s", name)}
	}
	if id.IsConstant() && id.Type() != UNDEFOBJ {
		return &Error{Message: fmt.Sprintf("Constant %s may not be changed", name)}
	}
	e.store[name] = val
	return NONE
}

func (e *Environment) Define(name string, val Object) Object {
	_, ok := e.store[name]
	if ok {
		return &Error{Message: fmt.Sprintf("Redefinition of %s not permitted", name)}
	}

	e.store[name] = val
	return NONE
}

func (e *Environment) Get(name string) Object {
	obj, ok := e.store[name]
	if !ok {
		if e.parent != nil {
			return e.parent.Get(name)
		}
		return nil
	}
	if obj.Type() == UNDEFOBJ {
		return &Error{Message: "Attempt to access undefined variable " + name}
	}
	return obj
}
